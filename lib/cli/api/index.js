module.exports.command = 'api'

module.exports.describe = 'Start an API unit.'

module.exports.builder = function(yargs) {
    return yargs
        .env('MSP_API')
        .strict()
        .option('connect-push', {
            describe: 'ZeroMQ PULL endpoint to connect to.'
            , type: 'string'
            , default: 'ipc:///tmp/app_proxy_pull'
        })
        .option('connect-sub', {
            describe: 'ZeroMQ PUB endpoint to connect to.'
            , type: 'string'
            , default: 'ipc:///tmp/app_proxy_pub'
        })
}

module.exports.handler = function(argv) {
    return require('../../units/api')({
        endpoints: {
            push: argv.connectPush,
            sub: argv.connectSub
        }
    })
}
