module.exports.command = 'provider'

module.exports.describe = 'Start a provider unit.'

module.exports.builder = (yargs) => {
    var os = require('os')
    var ip = require('my-local-ip')

    return yargs
        .strict()
        .env('MSP_PROVIDER')
        .option('adb-host', {
            describe: 'The ADB server host.',
            type: 'string',
            default: '127.0.0.1'
        })
        .option('adb-port', {
            describe: 'The ADB server port.',
            type: 'number',
            default: 5037
        })
        .option('allow-remote', {
            alias: 'R',
            describe: 'Whether to use tcp adb.',
            type: 'boolean',
            default: false
        })
        .option('connect-push', {
            alias: 'p',
            describe: 'Device-side ZeroMQ PULL endpoint to connect to.',
            type: 'string',
            default: 'ipc:///tmp/dev_proxy_pull'
        })
        .option('connect-sub', {
            alias: 's',
            describe: 'Device-side ZeroMQ PUB endpoint to connect to.',
            type: 'string',
            default: 'ipc:///tmp/dev_proxy_pub'
        })
        .option('screen-jpeg-quality', {
            describe: 'The JPG quality to use for the screen.',
            type: 'number',
            default: 80
        })
        .option('max-port', {
            describe: 'Highest port number for device workers to use.',
            type: 'number',
            default: 8700
        })
        .option('min-port', {
            describe: 'Lowest port number for device workers to use.',
            type: 'number',
            default: 7400
        })
}

module.exports.handler =  (argv) => {
    var path = require ('path')
    var cli = path.resolve(__dirname, '..')

    function range(from, to){
        var items = []
        for (var i = from; i <= to; ++i){
            items.push(i)
        }
        return items
    }

    return require ('../../units/provider')({
        killTimeout: 10000,
        ports:range(argv.minPort, argv.maxPort),
        allowRemote: argv.allowRemote,
        fork: function(device, ports, appiumPort){
            var fork = require('child_process').fork

            var args = [
                'device', device.id,
                '--screen-port', ports.shift(),
                '--appium-port', appiumPort,
                '--adb-host', argv.adbHost,
                '--adb-port', argv.adbPort,
                '--screen-jpeg-quality', argv.screenJpegQuality,
            ]
        
            return fork(cli, args)
        },
        endpoints: {
            sub: argv.connectSub,
            push: argv.connectPush
        },
        adbHost: argv.adbHost,
        adbPort: argv.adbPort
    })
}
