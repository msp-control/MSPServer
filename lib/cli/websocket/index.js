module.exports.command = 'websocket'

module.exports.describe = 'Start a websocket unit.'

module.exports.builder = function(yargs) {
  return yargs
    .env('MSP_API')
    .strict()
    .option('connect-push', {
      describe: 'ZeroMQ PULL endpoint to connect to.'
    , type: 'string'
    , default: 'ipc:///tmp/app_proxy_pull'
    })
    .option('connect-sub', {
      describe: 'ZeroMQ PUB endpoint to connect to.'
    , type: 'string'
    , default: 'ipc:///tmp/app_proxy_pub'
    })
    .option('port', {
      alias: 'p'
    , describe: 'The port to bind to.'
    , type: 'number'
    , default: 18080
    })
}

module.exports.handler = function(argv) {
  return require('../../units/websocket')({
    port: argv.port
  , endpoints: {
      sub: argv.connectSub
    , push: argv.connectPush
    }
  })
}