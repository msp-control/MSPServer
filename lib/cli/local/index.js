module.exports.command = 'local'

module.exports.describe = 'Start a complete local development environment.'

module.exports.builder = function(yargs) {
  var os = require('os')

  return yargs
    .env('MSP_LOCAL')
    .strict()
    .option('adb-host', {
      describe: 'The ADB server host.'
    , type: 'string'
    , default: '127.0.0.1'
    })
    .option('adb-port', {
      describe: 'The ADB server port.'
    , type: 'number'
    , default: 5037
    })
    .option('allow-remote', {
      alias: 'R'
    , describe: 'Whether to allow remote devices in MSP. Highly ' +
        'unrecommended due to almost unbelievable slowness on the ADB side ' +
        'and duplicate device issues when used locally while having a ' +
        'cable connected at the same time.'
    , type: 'boolean'
    })
    .option('bind-app-pub', {
      describe: 'The address to bind the app-side ZeroMQ PUB endpoint to.'
    , type: 'string'
    , default: 'ipc:///tmp/app_proxy_pub'
    })
    .option('bind-app-dealer', {
      describe: 'The address to bind the app-side ZeroMQ DEALER endpoint to.'
    , type: 'string'
    , default: 'ipc:///tmp/app_proxy_dealer'
    })
    .option('bind-app-pull', {
      describe: 'The address to bind the app-side ZeroMQ PULL endpoint to.'
    , type: 'string'
    , default: 'ipc:///tmp/app_proxy_pull'
    })
    .option('bind-dev-dealer', {
      describe: 'The address to bind the device-side ZeroMQ DEALER endpoint to.'
    , type: 'string'
    , default: 'ipc:///tmp/dev_proxy_dealer'
    })
    .option('bind-dev-pub', {
      describe: 'The address to bind the device-side ZeroMQ PUB endpoint to.'
    , type: 'string'
    , default: 'ipc:///tmp/dev_proxy_pub'
    })
    .option('bind-dev-pull', {
      describe: 'The address to bind the device-side ZeroMQ PULL endpoint to.'
    , type: 'string'
    , default: 'ipc:///tmp/dev_proxy_pull'
    })
    .option('provider', {
      describe: 'An easily identifiable name for the UI and/or log output.'
    , type: 'string'
    , default: os.hostname()
    })
    .option('provider-max-port', {
      describe: 'Highest port number for device workers to use.'
    , type: 'number'
    , default: 7700
    })
    .option('provider-min-port', {
      describe: 'Lowest port number for device workers to use.'
    , type: 'number'
    , default: 7400
    })
}

module.exports.handler = function(argv) {
  var util = require('util')
  var path = require('path')

  var Promise = require('bluebird')

  var logger = require('../../util/logger')
  var log = logger.createLogger('cli:local')
  var procutil = require('../../util/procutil')

  // Each forked process waits for signals to stop, and so we run over the
  // default limit of 10. So, it's not a leak, but a refactor wouldn't hurt.
  process.setMaxListeners(20)

  function run() {
    var procs = [
      // api
      procutil.fork(path.resolve(__dirname, '..'), [
          'api'
        , '--connect-push', argv.bindAppPull
        , '--connect-sub', argv.bindAppPub
        ])

      // websocket
     , procutil.fork(path.resolve(__dirname, '..'), [
          'api'
        , '--connect-push', argv.bindAppPull
        , '--connect-sub', argv.bindAppPub
        , '--port', argv.port
        ])

      // app proxy
    , procutil.fork(path.resolve(__dirname, '..'), [
          'proxy', 'app001'
        , '--bind-pub', argv.bindAppPub
        , '--bind-dealer', argv.bindAppDealer
        , '--bind-pull', argv.bindAppPull
        ])

      // device proxy
    , procutil.fork(path.resolve(__dirname, '..'), [
          'proxy', 'dev001'
        , '--bind-pub', argv.bindDevPub
        , '--bind-dealer', argv.bindDevDealer
        , '--bind-pull', argv.bindDevPull
        ])

      // processor one
    , procutil.fork(path.resolve(__dirname, '..'), [
          'processor', 'proc001'
        , '--connect-app-dealer', argv.bindAppDealer
        , '--connect-dev-dealer', argv.bindDevDealer
        ])

      // processor two
    , procutil.fork(path.resolve(__dirname, '..'), [
          'processor', 'proc002'
        , '--connect-app-dealer', argv.bindAppDealer
        , '--connect-dev-dealer', argv.bindDevDealer
        ])

      // provider
    , procutil.fork(path.resolve(__dirname, '..'), [
          'provider'
        , '--min-port', argv.providerMinPort
        , '--max-port', argv.providerMaxPort
        , '--connect-sub', argv.bindDevPub
        , '--connect-push', argv.bindDevPull
        , '--adb-host', argv.adbHost
        , '--adb-port', argv.adbPort
        ]
        .concat(argv.allowRemote ? ['--allow-remote'] : []))
    ]

    function shutdown() {
      log.info('Shutting down all child processes')
      procs.forEach(function(proc) {
        proc.cancel()
      })
      return Promise.settle(procs)
    }

    process.on('SIGINT', function() {
      log.info('Received SIGINT, waiting for processes to terminate')
    })

    process.on('SIGTERM', function() {
      log.info('Received SIGTERM, waiting for processes to terminate')
    })

    return Promise.all(procs)
      .then(function() {
        process.exit(0)
      })
      .catch(function(err) {
        log.fatal('Child process had an error', err.stack)
        return shutdown()
          .then(function() {
            process.exit(1)
          })
      })
  }

  return procutil.fork(__filename, ['migrate'])
    .done(run)
}
