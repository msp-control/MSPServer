module.exports.command = 'proxy'

module.exports.describe = 'Start app/device proxy unit.'

module.exports.builder = (yargs) => {
    var os = require ('os')

    return yargs
        .strict()
        .option('bind-dealer', {
              alias: 'd'
            , describe: 'The address to bind the ZeroMQ DEALER endpoint to.'
            , type: 'string'
            , default: 'ipc:///tmp/proxy_dealer'
        })
        .option('bind-pub', {
              alias: 'u'
            , describe: 'The address to bind the ZeroMQ PUB endpoint to.'
            , type: 'string'
            , default: 'ipc:///tmp/proxy_pub'
        })
        .option('bind-pull', {
              alias: 'p'
            , describe: 'The address to bind the ZeroMQ PULL endpoint to.'
            , type: 'string'
            , default: 'ipc:///tmp/proxy_pull'
        })

}

module.exports.handler = (argv) => {
    return require('../../units/proxy')({
        endpoints: {
              pub: argv.bindPub
            , dealer: argv.bindDealer
            , pull: argv.bindPull 
        }
    })
}