module.exports.command = 'processor [name]'

module.exports.describe = 'Start a processor unit.'

module.exports.builder = (yargs) => {
    return yargs
        .strict()
        .env('MSP_PROCESSOR')
        .option('connect-app-dealer', {
              alias: 'a'
            , describe: 'App-side ZeroMQ DEALER endpoint to connect to.'
            , type: 'string'
            , default: 'ipc:///tmp/app_proxy_dealer'
        })
        .option('connect-dev-dealer', {
              alias: 'd'
            , describe: 'Device-side ZeroMQ DEALER endpoint to connect to.'
            , type: 'string'
            , default: 'ipc:///tmp/dev_proxy_dealer'
        })
        
}

module.exports.handler = (argv) => {
    return require('../../units/processor')({
        name: argv.name
            , endpoints: {
              appDealer: argv.connectAppDealer
            , devDealer: argv.connectDevDealer
        }
    })
}
