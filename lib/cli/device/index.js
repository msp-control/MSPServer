var yargs = require('yargs')
module.exports.command = 'device <serial>'

module.exports.builder = function(yargs) {
    return yargs
        .strict()
        .option('adb-host', {
              describe: 'The ADB server host.'
            , type: 'string'
            , default: '127.0.0.1'
        })
        .option('adb-port', {
              describe: 'The ADB server port.'
            , type: 'number'
            , default: 5037
        })
        .option('appium-port', {
              describe: 'The appium server port.'
            , type: 'number'
            , demand: true
        })
        .option('connect-push', {
              alias: 'p'
            , describe: 'ZeroMQ PULL endpoint to connect to.'
            , type: 'string'
            , default: 'ipc:///tmp/dev_proxy_pull'
        })
        .option('connect-sub', {
              alias: 's'
            , describe: 'ZeroMQ PUB endpoint to connect to.'
            , type: 'string'
            , default: 'ipc:///tmp/dev_proxy_pub'
        })
        .option('screen-jpeg-quality', {
              describe: 'The JPG quality to use for the screen.'
            , type: 'number'
            , default: 80
        })
        .option('screen-port', {
              describe: 'Port allocated to the screen WebSocket.'
            , type: 'number'
            , demand: true
        })
        .option('boot-complete-timeout', {
              describe: 'How long to wait for boot to complete during device setup.'
            , type: 'number'
            , default: 60000
        })
}

module.exports.handler = function(argv) {
    return require('../../units/device')({
        serial: argv.serial
        , provider: argv.provider
        , endpoints: {
              sub: argv.connectSub
            , push: argv.connectPush
        }
        , adbHost: argv.adbHost
        , adbPort: argv.adbPort
        , screenJpegQuality: argv.screenJpegQuality
        , screenPort: argv.screenPort
        , appiumPort: argv.appiumPort
        , bootCompleteTimeout: argv.bootCompleteTimeout
    })
}