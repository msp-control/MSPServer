var EventEmitter = require('eventemitter3')
var util = require('util')

var log = require('../util/logger').createLogger('wire:router')
var on = EventEmitter.prototype.on

function Router() {
  if (!(this instanceof Router)) {
    return new Router()
  }

  EventEmitter.call(this)
}

util.inherits(Router, EventEmitter)

// Router.prototype.on = function(message, handler) {
//   return on.call(this, message.$code, handler)
// }

// Router.prototype.removeListener = function(message, handler) {
//   return EventEmitter.prototype.removeListener.call(
//     this
//   , message.$code
//   , handler
//   )
// }

Router.prototype.handler = function() {
  return function(channel, data) {
    var wrapper = JSON.parse(data)
    this.emit(
      wrapper.type
    , channel
    , wrapper
    )
    this.emit(
      'message'
    , channel
    , wrapper
    )
  }.bind(this)
}

module.exports = Router
