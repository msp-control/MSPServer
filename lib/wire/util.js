var wireutil = {
  reply: function(id, type) {
    return {
      shellMsg: (msg) => {
        return JSON.stringify({id: id, type: type, ret_code: 0, ret_msg: msg})
      }
    , catchErr: (err) => {
        return JSON.stringify({id: id, type: type,  ret_code: -1, ret_msg: err})
      }
    , okay: (msg) => {
        return JSON.stringify({id: id, type: type,  ret_code: 0, ret_msg: msg})
      }
    , fail: (msg) => {
        return JSON.stringify({id: id, type: type,  ret_code: -1, ret_msg: msg})
      }
    }
  }
}

module.exports = wireutil