var loadingNotes = {
        success: $L('Profiles data is loading...'),
        failed: $L('Failed to load phones data')
    },
    needReset = true,
    execTasks = false,
    ExecTaskWin = false,
    winCloseTag = false,
    googleMapLoadFlag = false;
var socket;
//var interfaceUrl = "http://120.234.25.82:7647/";
var interfaceUrl = window.location.protocol+'//'+window.location.host + '/';//"http://192.168.57.123:13002/";
var deviceOperationType = {wifi:1000, airplane_mode:1001, screen:1002, gps:1003, import_files:1004, import_contacts:1005, clear_contacts:1006, clear_images:1007, reboot:1008, shutdown:1009, brightness:1010, install_apk:1011, uninstall_apk:1012, screen_capture:1013, shell:1014, press_home:1015, press_back:1016, press_menu:1017, get_apps_list:1018, clear_cache:1020, quit_apps:1021, message_blank:1022, send_short_message:1023, import_contacts_by_text:1024,  bind_virual_device_identity:1026, touch_start:2000, touch_stop:2001, touch_down:2002, touch_move:2003, touch_up:2004, touch_commit:2005, touch_reset:2006, touch_tap:2007, input_text:3000, input_enter:3001, input_back:3002, input_wrap:3003};
//swfupload


function formatData(data) {
    var columns = data[0]['columns'],
        values = data[0]['values'],
        dataObj = [];
    for (var i = 0; i < values.length; i++) {
        var value = values[i];
        var obj = {};
        for(var j = 0; j < value.length; j++) {
            obj[columns[j]] = value[j];
        }
        dataObj.push(obj);
    }
    return dataObj;
}
function getData(table, page, psize, sortf, sort, type) {
    if(!page) page = 1;
    if(!psize) psize = 30;
    var start = parseInt(psize * (page - 1));
    var countSql = "select count(*) from " + table, sql = "select * from " + table;
    if(type) {
        var search = $("#search").serializeArray();
        if(search[0].value) {
            countSql += " where status = " + "'" + search[0].value + "'";
            sql += " where status = " + "'" + search[0].value + "'";
        }
    }
    sql += " limit " + start + "," + psize;
    var data = readDB(sql),
        totalArr = readDB(countSql),
        dataObj = {
            "data":[],
            "total": totalArr[0]['values'][0][0],
            "page": page
        };
    if(!data.length) return dataObj;
    var dataDetail = formatData(data);
    dataObj.data = dataDetail;
    return dataObj;
}

//设备实时屏幕， websocket连接
function deviceScreenWS(domId, port){
    var url = 'wss://'+window.location.host+'/screen/1/'+port;//'ws://192.168.57.123:' + port;
    var socket = new WebSocket(url);
    socket.onopen = function(){
        if(socket.readyState==1){
            socket.send('size 120x170');
            socket.send('on');
        }
    }
    socket.onclose = function(){
        socket = false;
        console.log('close websocket connection');
    }
    socket.onmessage = function(message){
        //console.log(message);
        //eval('var data = '+msg.data);
        //document.getElementById('screen-img').src = data.nrong;
        //$('#device-info .device-info-screen img').attr('src', 'data:image/jpeg;base64,'+data.nrong);
        var blob = new Blob([message.data], {type: 'image/jpeg'});
        var URL = window.URL || window.webkitURL;
        var u = URL.createObjectURL(blob);
        $(domId).find('.screen-img img').attr('src', u);
        $(domId).find('.screen-img img').one('load', function() {
            // console.log('revokeObjectURL')
            URL.revokeObjectURL(u)
        })

    }

    return socket
}

function fetchDeviceList(page, psize, sortf, sort, type) {
    qwp.get({
        quiet: true,
        url: interfaceUrl + 'api/device_infos',
        params: {},
        fn: function(res){
            var data = {data:[], total:0, page:1};
            var activeDomId = [];
            for(var i=0; i<res.length; i++){
                var record = res[i];
                if(!record.status){
                    continue;
                }
                if(!record.loc_x || !record.loc_y){
                    continue;
                }
                data.data.push({
                    serial_num: record.loc_x + ',' + record.loc_y,
                    memory_percent: record.memory_percent,
                    net_signal: record.network
                });
                var domIndex = record.loc_x*10 + record.loc_y;
                var domId = '#device-' + domIndex;
                activeDomId.push(domIndex);
                // var imgSrc = interfaceUrl+"images/screens/"+record.id+".jpg?"+Math.random();
                //$(domId).find('.screen-img').html("<img src='" + imgSrc + "' onclick='deviceInfo("+ record.id +")' />");
                var screenSock = $(domId).find('.screen-img img').data('screen_sock')
                if (!screenSock || screenSock.readyState !== 1) {
                    if (screenSock) {
                        screenSock.close()
                        screenSock = null
                    }

                    screenSock = deviceScreenWS(domId, record.screen_port)
                    $(domId).find('.screen-img img').data('screen_sock', screenSock)
                }

                $(domId).find('.screen-img img')
                // .attr('src', imgSrc)
                .attr('onclick', 'deviceInfo('+ record.id +')');
                $(domId).find('.screen-status .phone-status').removeClass('disabled');
            }
            data.total = res.length;
            qwp.table.update('device', data, page, psize, sortf, sort);
            for(var i=1; i<=10; i++){
                for(var j=1; j<=5; j++){
                    var domIndex = i*10 + j;
                    if(activeDomId.indexOf(domIndex) == -1){
                        var domId = '#device-' + domIndex;
                        $(domId).find('.screen-img img').attr('src', 'img/screen-default.jpg').attr('onclick', '');
                        $(domId).find('.screen-status .phone-status').addClass('disabled');
                    }
                }
            }
        }
    });
}

$('#check-correspondence-btn').click(function(){ checkCorrespondence(); });
function checkCorrespondence(){
    qwp.get({
        quiet: true,
        url: 'http://localhost/demo/interface.php',
        params: {},
        fn: function(res){
            var data = {data:[], total:0, page:1};
            for(var i=0; i<res.length; i++){
                var record = res[i];
                data.data.push({
                    serial_num: record.loc_x + ',' + record.loc_y,
                    correspondence_plugin: '已安装',
                    assist_plugin: '已安装',
                    position_plugin: '已安装',
                    correspondence_status: '正常'
                });
            }
            data.total = res.length;
            qwp.table.update('check-correspondence', data, 1, null, null, null);
            $('#check-correspondence').modal();
        }
    });
}
//接入设备
$('#access-device-btn').click(function(){ accessDevice(); });
function accessDevice(){
    qwp.get({
        quiet: true,
        url: interfaceUrl + 'api/device_infos',
        params: {},
        fn: function(res){
            var data = {data:[], total:0, page:1};
            for(var i=0; i<res.length; i++){
                var record = res[i];
                if(!record.status){
                    continue;
                }
                if(!record.loc_x || !record.loc_y){
                    data.data.push({
                        status: $h.span('', {class: 'access-device-status device-status'+ record.status}),
                        model: record.model,
                        operate: $h.button('接入', {class:'btn btn-success btn-xs', onclick:'showDeviceAssignLocation(' + record.id + ')'})
                    });
                }
            }
            data.total = res.length;
            qwp.table.update('access-device', data, 1, null, null, null);
            $('span.access-device-status.disabled').parent('td').prev().find('input[type="checkbox"]').hide();
            $('#access-device').modal();
        }
    });
}
function showDeviceAssignLocation(id){
    if(id){
        $('input#location-device-id').val(id);
        $('#access-device-assign-location').modal();
    }
}
function saveDeviceLocation(){
    var deviceId = $('#location-device-id').val();
    var locX = $('#device-location-x').val();
    var locY = $('#device-location-y').val();
    if(!deviceId || !locX || !locY){
        alert('数据错误！！');
        return;
    }
    var params = {
        loc_x: locX,
        loc_y: locY
    };
    qwp.ajax({
        quite: false,
        url: interfaceUrl + 'api/device_infos/' + deviceId,
        type: 'put',
        params: params,
        fn: function(res){
            if(res.id){
                $('#access-device-assign-location').modal('hide');
                window.location.reload();
            }else{
                alert('数据错误');
            }
        }
    });
}
//移除设备
$('#remove-device-btn').click(function(){ removeDevice(); });
function removeDevice(){
    qwp.get({
        quiet: true,
        url: interfaceUrl + 'api/device_infos',
        params: {},
        fn: function(res){
            var data = {data:[], total:0, page:1};
            for(var i=0; i<res.length; i++){
                var record = res[i];
                if(!record.loc_x || !record.loc_y){
                    continue;
                }
                data.data.push({
                    serial_num: record.loc_x + ',' + record.loc_y,
                    model: record.model,
                    operate: $h.button(
                        '移除',
                        {
                            class:'btn btn-success btn-xs',
                            onclick:'doRemoveDevice(' + record.id + ')'
                        }
                    )
                });
            }
            data.total = res.length;
            qwp.table.update('remove-device', data, 1, null, null, null);
            $('#remove-device').modal();
        }
    });
}
function doRemoveDevice(id){
    if(id){
        qwp.ajax({
            quite: false,
            url: interfaceUrl + 'api/device_infos/' + id,
            type: 'put',
            params: {loc_x:0, loc_y:0},
            fn: function(res){
                if(res.id){
                    $('#remove-device').modal('hide');
                    window.location.reload();
                }else{
                    alert('数据错误');
                }
            }
        });
    }
}

function convertProfilesData(r) {
    r.used = $h.img({src:'img/'+(r.used == 'y' ? 'delete' : 'success') + '.png'});
    r.status = r.status == 'n' ? 'Ready' : r.status == 's' ? 'Success' : 'Export';
}
qwp.r(function(){
    qwp.table.create('#list-device', 'device', {
        fetchData: 'fetchDeviceList',
        dataConvertor: convertProfilesData,
        selectable: false,
        hideOps: true,
        header:{
            "names":[
                ["serial_num","序号",5],
                ["memory_percent","内存占用",10],
                ["net_signal","网络信号",10]
            ]
        }
    });
    for(var i=1; i<=10; i++){
        for(var j=1; j<=5; j++){
            var screenHtml = "<div class='phone-screen' id='device-" + (i*10+j) + "'>" +
                "<div class='screen-img'><img src='img/screen-default.jpg' /></div>" +
                "<div class='screen-status'>" +
                "<span class='screen-pos'>"+ i + "," + j +"</span>" +
                "<span class='phone-status disabled'></span>" +
                "</div>" +
                "</div>";
            $('#phone-screen-container').append(screenHtml);
        }
    }
    fetchDeviceList();
    window.setInterval(function(){fetchDeviceList();}, 5000);

    //检测通信
    qwp.table.create('#list-check-correspondence', 'check-correspondence', {
        selectable: true,
        hideOps: true,
        header: {
            "names":[
                ['serial_num','序号',3],
                ['correspondence_plugin','通讯插件',5],
                ['assist_plugin','辅助插件',5],
                ['position_plugin','定位插件',5],
                ['correspondence_status','通讯状态',5]
            ]
        }
    });
    //导入图片列表
    qwp.table.create('#list-import-picture', 'import-picture', {
        selectable: true,
        btns: {
            addons: [{
                txt: $L('导入图片'),
                style: 'border-radius:3px;',
                click: 'doImportPicture'
            }]
        },
        topCols: {
            left: 7,
            right: 5
        },
        header: {
            "names": [
                ["name", $L('File Name')]
            ]
        }
    });
    //通讯录列表
    qwp.table.create('#list-contacts-book', 'contacts-book', {
        selectable: true,
        btns: {
            addons: [{
                txt: $L('Create'),
                class: 'btn-primary import-btn',
                icon: 'import',
                click: 'createContacts',
                tooltip: $L('Click to import new contacts')
            },{
                txt: $L('Import'),
                class: 'btn-primary import-btn',
                icon: 'import',
                click: 'importContacts',
                tooltip: $L('Click to import new contacts')
            },{
                txt: $L('Generate'),
                class: 'btn-primary',
                icon: 'glyphicon glyphicon-plus',
                click: 'generateContacts',
                tooltip: $L('Click to generate contacts')
            }]
        },
        topCols: {
            left: 7,
            right: 5
        },
        header: {
            "names": [
                ["name", "姓名", 5],
                ["number", "手机", 5]
            ]
        }
    });
    //生成通讯录 区段列表
    qwp.table.create('#list-generate-section', 'generate-section', {
        selectable: true,
        radio: true,
        hideOps: true,
        header: {
            "names": [
                ["section", "号码段", 6],
                ["operator", "运行商", 5]
            ]
        }
    });
    //生成通讯录 列表
    qwp.table.create('#list-generate-contacts-book', 'generate-contacts-book', {
        selectable: false,
        hideOps: true,
        header: {
            "names": [
                ["phone", "手机号码", 5]
            ]
        }
    });
    var $distpicker = $('#distpicker');
    $distpicker.distpicker({
        province: '福建省',
        city: '厦门市'
    });
    //apk 列表
    qwp.table.create('#list-device-apk', 'device-apk', {
        selectable: false,
        hideOps: false,
        btns: {
            addons: [{
                txt: $L('Install'),
                class: 'btn-primary import-btn',
                icon: 'import',
                click: 'importApk',
                tooltip: $L('Click to install new apk')
            }]
        },
        header:{
            "names":[
                ["name","APK",5],
                ["version",'版本',5],
                ["operate","动作",3]
            ]
        }
    });
    //虚拟身份列表
    qwp.table.create('#list-virtual-identity', 'virtual-identity', {
        fetchData: 'fetchVirtualIdentityData',
        selectable: true,
        radio: true,
        btns: {
            addons:[{
                txt:$L('Bind'),
                'class':'btn-primary import-btn',
                icon:'glyphicon glyphicon-saved',
                click: 'bindVirtualIdentityData',
                tooltip:$L('Click to bind the virtual device information')
            }]
        },
        topCols:{
            left:5,
            right:7
        },
        header: {
            "names":[
                ["id","ID",2],
                ["first_name", "姓", 2],
                ["last_name", "名", 2],
                ["age","年龄",2],
                ["gender","性别",2],
                ["work","职业",2],
                ["hobby","爱好",2],
                ["phonenumber","手机",2],
                ["IPs","IP",2],
                ["idcard","证件",2]
            ]
        }
    });
    //接入设备
    qwp.table.create('#list-access-device', 'access-device', {
        selectable: false,
        radio: true,
        hideOps: true,
        header:{
            "names":[
                ["status","状态",2],
                ["model","设备型号",5],
                ["operate","动作",3]
            ]
        }
    });
    //移除设备
    qwp.table.create('#list-remove-device', 'remove-device', {
        selectable: false,
        hideOps: true,
        header:{
            "names":[
                ["serial_num","序号",2],
                ["model","设备型号",5],
                ["operate","动作",3]
            ]
        }
    });
    //uploadify
    var $input = $("#upload_field").html5_upload({
        //url: 'http://localhost/demo/upload.php',
        url: interfaceUrl+'api/containers/test1/upload',
        sendBoundary: true,
        onStart: function(event, total) {
            return true;
        },
        onProgress: function(event, progress, name, number, total) {
            //console.log(progress, number);
        },
        setName: function(text) {
            $("#progress_report_name").text(text);
        },
        setStatus: function(text) {
            $("#progress_report_status").text(text);
        },
        setProgress: function(val) {
            $("#progress_report_bar").css('width', Math.ceil(val*100)+"%");
        },
        onFinishOne: function(event, response, name, number, total) {
            var response = JSON.parse(response);
            var taskType = $('#file_task_type').val();
            if(response.result && response.result.files && taskType){
                var files = response.result.files['user_file[]'];
                for(var i=0; i<files.length; i++){
                    var file = files[i];
                    if(taskType == deviceOperationType.install_apk){
                        createTask(taskType, {'file':{'container':file.container, 'name':file.name}}, true, showApkList);
                    }else if(taskType == deviceOperationType.import_contacts){
                        createTask(taskType, {'file':{'container':file.container, 'name':file.name}}, true, contactsBook);
                    }else if(taskType == deviceOperationType.import_files){
                        createTask(taskType, {'file':{'container':file.container, 'name':file.name}}, true, importPicture);
                    }else{
                        createTask(taskType, {'file':{'container':file.container, 'name':file.name}}, true);
                    }
                }
                $('#import-file-dialog').modal('hide');
            }
        },
        onError: function(event, name, error) {
            alert('error while uploading file ' + name);
        }
    });
});

//var webSock = io.connect('wss://'+window.location.host+'/socketio');
var webSock = new WebSocket('wss://'+window.location.host+'/socketio')
//下发任务
function createTask(type, args, dealResult, callbackFn){
    var devs = [];
    var deviceId = $('#device-info input[name="device-id"]').val()
    if(!$('#operate-all-device').is(':checked')){
        devs.push(deviceId);
    }
    var resultFlag = true;
    var params = {
        devs: devs,
        type: type,
        args: JSON.stringify(args)
    };

    // if (type >= 2000 && type <= 2007) {
    if (0) {
        webSock.send(JSON.stringify({devs:devs,type:type,args:args}))
    } else {
        //console.log(params);
        qwp.ajax({
            quite: true,
            url: interfaceUrl + 'api/task_infos/createTask',
            type: 'post',
            params: params,
            fn: function(res){
                if(res.success){
                    console.log(type + '任务下发成功！！');
                    if(dealResult && res.id){
                        $('#loading .loading-text').text('任务正在下发,请稍候 ...');
                        $('#loading').modal('show');
                        var intervalFlag = window.setInterval(function(){
                            qwp.ajax({
                                quite: true,
                                url: interfaceUrl + 'api/task_infos/' + res.id[0],
                                type: 'get',
                                fn: function(res){
                                    if(res.ret_code == -1 || res.ret_code == 0){
                                        window.clearInterval(intervalFlag);
                                        if(res.ret_code == -1){
                                            $('#loading .loading-text').text('任务失败:' + res.ret_msg);
                                        }else if(res.ret_code == 0){
                                            console.log('任务成功，taskinfo');
                                            $('#loading').modal('hide');
                                            if(callbackFn){
                                                callbackFn.call();
                                            }
                                        }
                                    }
                                }
                            });
                        }, 2000);
                    }
                }else{
                    resultFlag = false;
                    console.log(type + '操作失败！！');
                    if(dealResult){
                        alert(type + '操作失败！！');
                    }
                }
            }
        });
    }
    return resultFlag;
}
