(function(){
    //输入框操作
    var cpLock = false;
    $('#screen-input').on('compositionstart', function () {
        console.log('compositionstart');
        cpLock = true;
    });
    $('#screen-input').on('compositionend', function () {
        console.log('compositionend');
        cpLock = false;
        var text = $('#screen-input').val();
        console.log(text);
        if(text){
            createTask(deviceOperationType.input_text, {"char":text});
            $('#screen-input').val('');
        }
    });
    $('#screen-input').bind('input propertychange', function(){
        if (cpLock) {
            return;
        }
        var text = $('#screen-input').val();
        console.log(text);
        if(text){
            createTask(deviceOperationType.input_text, {"char":text});
            $('#screen-input').val('');
        }
    });

    $(window).keydown(function(event){
        if(!$('#device-info').is(':visible')){
            return;
        }
        $('#screen-input').show();
        $('#screen-input').css({top:(-120)+'px', left:(0)+'px'});
        $('#screen-input').focus();
        switch(event.keyCode){
            case 8:
                createTask(deviceOperationType.input_back, {});
                break;
            case 13:
                if(event.ctrlKey){
                    createTask(deviceOperationType.input_wrap, {});
                }else{
                    createTask(deviceOperationType.input_enter, {});
                }
                break;
        }
    });

    //设备信息-屏幕操作
    //触摸滑动，开始
    var touchTaskSeq = 0;
    $('#device-info-screen-mask').mousedown(function(event){
        $(this).attr('pressFlag', true);
        var screenWidth = $(this).width();
        var screenHeight = $(this).height();
        var offsetX = event.offsetX;
        var offsetY = event.offsetY;
        var relativeX = offsetX / screenWidth;
        var relativeY = offsetY / screenHeight;
        touchTaskSeq = 0;
        createTask(deviceOperationType.touch_start, {seq:touchTaskSeq++});
        createTask(deviceOperationType.touch_down, {seq:touchTaskSeq++, x:relativeX, y:relativeY, contact:1});
        createTask(deviceOperationType.touch_commit, {seq:touchTaskSeq++});
    });
    //触摸滑动， 结束
    $('#device-info-screen-mask').bind('mouseup', function(event){
        //输入框显示
        var offsetX = event.offsetX;
        var offsetY = event.offsetY;
        $('#screen-input').show();
        $('#screen-input').css({top:(-120)+'px', left:(0)+'px'});
        $('#screen-input').focus();

        if(!$(this).attr('pressFlag') || $(this).attr('pressFlag')=='false'){
            return;
        }
        $(this).attr('pressFlag', false);
        createTask(deviceOperationType.touch_up, {seq:touchTaskSeq++, contact:1});
        createTask(deviceOperationType.touch_commit, {seq:touchTaskSeq++});
        createTask(deviceOperationType.touch_stop, {seq:touchTaskSeq++});

    });
    //触摸滑动，滑动
    var canSend = true
    var canSendTimer = false
    $('#device-info-screen-mask').mousemove(function(event){
        if(!$(this).attr('pressFlag') || $(this).attr('pressFlag')=='false'){
            return;
        }

        if (!canSend) {
          if (canSendTimer){
            clearTimeout(canSendTimer)
          }
          canSendTimer = setTimeout(() => {canSend = true}, 10)
          return;
        }

        canSend = false

        var screenWidth = $(this).width();
        var screenHeight = $(this).height();
        var offsetX = event.offsetX;
        var offsetY = event.offsetY;
        var relativeX = offsetX / screenWidth;
        var relativeY = offsetY / screenHeight;
        createTask(deviceOperationType.touch_move, {seq:touchTaskSeq++, x:relativeX, y:relativeY, contact:1});
        createTask(deviceOperationType.touch_commit, {seq:touchTaskSeq++});
    });
    //触摸滑动， 离开区域
    $('#device-info-screen-mask').bind('mouseleave', function(event){
        if(!$(this).attr('pressFlag') || $(this).attr('pressFlag')=='false'){
            return;
        }
        $(this).attr('pressFlag', false);
        createTask(deviceOperationType.touch_up, {seq:touchTaskSeq++, contact:1});
        createTask(deviceOperationType.touch_commit, {seq:touchTaskSeq++});
        createTask(deviceOperationType.touch_stop, {seq:touchTaskSeq++});

    });
})()
