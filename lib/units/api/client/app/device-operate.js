//设备信息
var bigScreenSock

function deviceInfo(id){
    qwp.get({
        quiet: true,
        url: interfaceUrl + 'api/device_infos/' + id,
        params: {},
        fn: function(res){
            if(!res){
                return;
            }
            $('#device-info input[name="device-id"]').val(res.id);
            $('#device-info #assist-operation-container form#real-deviceinfo-form input').val('');
            $('#assist-operation-container input#device-id').val(res.loc_x + ',' + res.loc_y);
            $('#assist-operation-container input#device-model').val(res.model);
            $('#assist-operation-container input#device-serial').val(res.serial);
            $('#assist-operation-container input#device-imei').val(res.imei);
            $('#assist-operation-container input#device-imsi').val(res.imsi);
            $('#assist-operation-container input#device-mac').val(res.mac);
            if(res.wifi_status){
                $('#device-info #set-wifi-btn').text('关闭WIFI').attr('action_type', 'close');
            }else{
                $('#device-info #set-wifi-btn').text('打开WIFI').attr('action_type', 'open');
            }
            if(res.airplane_mode_status){
                $('#device-info #set-airplane-btn').text('关闭飞行模式').attr('action_type', 'close');
            }else{
                $('#device-info #set-airplane-btn').text('打开飞行模式').attr('action_type', 'open');
            }
            $('#device-info').modal();
            oneDeviceScreenWS(res.screen_port);
            window.setTimeout(function(){
                var screenWidth = $('#device-info .device-info-screen').width();
                var screenHeight = $('#device-info .device-info-screen').height();
                var screenPos = $('#device-info .device-info-screen').position();
                $('#device-info-screen-mask').css({
                    'width': screenWidth + 'px',
                    'height': screenHeight + 'px',
                    'left': screenPos.left + 'px',
                    'top': screenPos.top + 'px'
                });
            }, 3000);

        }
    });
    //getVirtualIdentityList();
}
$('#show-virtual-identity-btn').click(function(){
    getVirtualIdentityList();
});
function getVirtualIdentityList(){
    var id = $('#device-info input[name="device-id"]').val()
    qwp.get({
        quiet: true,
        url: interfaceUrl + 'api/device_infos/' + id + '/virtualIdentityInfos',
        params: {},
        fn: function(res){
            if(!res){
                return;
            }
            for(var i=0; i<res.length; i++){
                var record = res[i];
                var html = "<option ";
                html += " value=" + i;
                html += " idN=" + record.id;
                html += " age=" + record.age;
                html += " gender=" + record.gender;
                html += " work=" + record.work;
                html += " hobby=" + record.hobby;
                html += " phonenumber=" + record.phonenumber;
                html += " IPs=" + record.IPs;
                html += " idcard=" + record.idcard;
                html += " >" + record.first_name + record.last_name + "</option>";
                $("#virtual-identity-table select[name='vitual-user-list']").append(html);
            }
            virtualIdentitySelectChange(0);
        }
    });
}
$("#virtual-identity-table select[name='vitual-user-list']").change(function(){
    virtualIdentitySelectChange($(this).val());
});
function virtualIdentitySelectChange(value){
    $("#virtual-identity-table td.item-value").text('');
    var obj = $("#virtual-identity-table select[name='vitual-user-list']").find("option[value='" + value + "']");
    if(obj){
        $("#virtual-identity-table td[name='id']").text(obj.attr('idN'));
        $("#virtual-identity-table td[name='age']").text(obj.attr('age'));
        $("#virtual-identity-table td[name='gender']").text(obj.attr('gender'));
        $("#virtual-identity-table td[name='work']").text(obj.attr('work'));
        $("#virtual-identity-table td[name='hobby']").text(obj.attr('hobby'));
        $("#virtual-identity-table td[name='phonenumber']").text(obj.attr('phonenumber'));
        $("#virtual-identity-table td[name='IPs']").text(obj.attr('IPs'));
        $("#virtual-identity-table td[name='idcard']").text(obj.attr('idcard'));
        qwp.get({
            quiet: true,
            url: interfaceUrl + 'api/virtual_identity_infos/' + obj.attr('idN') + '/virtualDeviceinfo/',
            params: {},
            fn: function(res){
                if(!res){
                    return;
                }
                console.log(res);
                $("#virtual-identity-table td[name='imei']").text(res.imei);
                $("#virtual-identity-table td[name='imsi']").text(res.imsi);
                $("#virtual-identity-table td[name='mac']").text(res.mac);
            }
        });
    }
}
$('#gen-virtualdeviceinfo-btn').click(function(){
    generateVirtualDeviceinfo();
});
function generateVirtualDeviceinfo(){
    qwp.get({
        quite: true,
        url: interfaceUrl + 'api/tools/genVirDevInfo',
        params: {},
        fn: function(res){
            if(!res && !res.genInfo){
                return;
            }
            res = res.genInfo;
            $('#device-info #assist-operation-container form#virtual-deviceinfo-form input').val('');
            $('#assist-operation-container input#v-device-id').val($('#assist-operation-container input#device-id').val());
            $('#assist-operation-container input#v-device-model').val(res.model);
            $('#assist-operation-container input#v-device-serial').val(res.serial);
            $('#assist-operation-container input#v-device-imei').val(res.imei);
            $('#assist-operation-container input#v-device-imsi').val(res.imsi);
            $('#assist-operation-container input#v-device-mac').val(res.mac);
            $('#device-info #assist-operation-container form#virtual-deviceinfo-form').show();
            $('#bind-identityinfo-btn').attr('disabled', false);
        }
    });
}
//设备信息 -- 虚拟身份信息窗口
$('#bind-identityinfo-btn').click(function(){ virtualIdentity(); });
function virtualIdentity(){
    fetchVirtualIdentityData();
}
function fetchVirtualIdentityData(page, psize, sortf, sort, type){
    qwp.get({
        quiet: true,
        url: interfaceUrl + 'api/virtual_identity_infos/',
        params: {},
        fn: function(res){
            if(!res){ return; }
            var data = {data:[], total:0, page:1};
            for(var i=0; i<res.length; i++){
                var record = res[i];
                data.data.push({
                    id: record.id,
                    first_name: record.first_name,
                    last_name: record.last_name,
                    gender: record.gender,
                    age: record.age,
                    work: record.work,
                    hobby: record.hobby,
                    phonenumber: record.phonenumber,
                    IPs: record.IPs,
                    idcard: record.idcard
                });
            }
            data.total = res.length;
            qwp.table.update('virtual-identity', data, page, psize, sortf, sort);
            $('#virtual-identity-bind').modal();
        }
    });
}
function bindVirtualIdentityData(){
    var selectedId = qwp.table.selectedIDs('virtual-identity');
    var identityData = qwp.table.data('virtual-identity');
    for(var i=0; i<identityData.data.length; i++){
        var record = identityData.data[i];
        if(record.id == selectedId){
            break;
        }
    }
    record.virtualDeviceInfo = {
        model: $('#assist-operation-container input#v-device-model').val(),
        serial: $('#assist-operation-container input#v-device-serial').val(),
        imei: $('#assist-operation-container input#v-device-imei').val(),
        imsi: $('#assist-operation-container input#v-device-imsi').val(),
        mac: $('#assist-operation-container input#v-device-mac').val()
    };
    var params = {"virtualIdentityInfo": record};
    createTask(deviceOperationType.bind_virual_device_identity, params);
    $('#virtual-identity-bind').modal('hide');
}
//定位信息  -- 已存在定位列表
function locationInfo(){
    var positionArr = [{
        name: 'position01',
        polygon: [{
            lat: 22.273211999702166,
            lng: 114.12998914718628
        },{
            lat: 22.27893059841188,
            lng: 114.22199964523315
        },{
            lat: 22.199484707938296,
            lng: 114.21856641769409
        }]
    },{
        name: 'position02',
        polygon: [{
            lat: 39.68605343225987,
            lng: 116.50915145874023
        },{
            lat: 39.6606850221923,
            lng: 117.17931747436523
        },{
            lat: 39.41073305508496,
            lng: 116.89367294311523
        },{
            lat: 39.317300373271024,
            lng: 116.11913681030273
        },{
            lat: 39.58452390500424,
            lng: 115.99828720092773
        }]
    },{
        name: 'position03'
    },{
        name: 'position04'
    },{
        name: 'position05'
    },{
        name: 'position06'
    }];
    var positionListHtml = "";
    for(var i=0; i<positionArr.length; i++){
        var pos = positionArr[i];
        positionListHtml += "<li onclick='showPosition(" + JSON.stringify(pos) + ")'>" + pos.name + "</li>";
    }
    $("#position-list ul").html(positionListHtml);
    $('#location-info').modal();
    if(!googleMapLoadFlag){
        window.setTimeout(function(){ initializeGoogleMap(); }, 500);
    }
}
function saveLocationInfo(){
    var posName = $("#position-form #position-name").val();
    var points = $("#position-form #position-polygon").val();
    if(!posName || !points){
        alert('请设置，位置名称， 选择位置区域！！');
        return;
    }
    createTask(deviceOperationType.gps, points);
}
//导入图片
function importPicture(){
    var deviceId = $('#device-info input[name="device-id"]').val()
    qwp.get({
        quiet: true,
        url: interfaceUrl + 'api/device_infos/' + deviceId + '/getPicturesList',
        params: {},
        fn: function(res){
            console.log(res);
            var data = {data:[], total:0, page:1};
            var res = res.result;
            for(var i=0; i<res.length; i++){
                var record = res[i];
                data.data.push({
                    name: record.name
                });
            }
            data.total = res.length;
            qwp.table.update('import-picture', data, 1, null, null, null);
        }
    });
    $('#import-picture').modal();
}
function doImportPicture(){
    $('#file_task_type').val(deviceOperationType.import_files);
    $('#import-file-dialog').modal();
}
//通讯录
$('#contacts-book-btn').click(function(){ contactsBook(); });
function contactsBook(){
    var deviceId = $('#device-info input[name="device-id"]').val()
    qwp.get({
        quiet: true,
        url: interfaceUrl + 'api/device_infos/' + deviceId + '/getContactsList',
        params: {},
        fn: function(res){
            console.log(res);
            var data = {data:[], total:0, page:1};
            var res = res.result;
            for(var i=0; i<res.length; i++){
                var record = res[i];
                data.data.push({
                    name: record.name,
                    number: record.number
                });
            }
            data.total = res.length;
            qwp.table.update('contacts-book', data, 1, null, null, null);
        }
    });
    $('#contacts-book').modal();
}
function importContacts(){
    $('#file_task_type').val(deviceOperationType.import_contacts);
    $('#import-file-dialog').modal();
}
function createContacts(){
    $('#create-contacts-book').modal();
}
function doCreateContacts(){
    var textArea = $('#contacts-list-area').val();
    console.log(textArea);
    var textAreaArr = textArea.split("\n");
    var contactsArr = [];
    for(var i=0; i<textAreaArr.length; i++){
        var record = textAreaArr[i];
        var recordArr = record.split(" ");
        if(recordArr.length == 2 && recordArr[0] && recordArr[1]){
            contactsArr.push({"name":recordArr[0], "number":recordArr[1]});
        }
    }
    if(contactsArr.length == 0){
        alert('请输入要导入的联系方式');
        return;
    }
    createTask(deviceOperationType.import_contacts_by_text, contactsArr, true, function(){
        $('#create-contacts-book').modal('hide');
        contactsBook();
    });
}
function generateContacts(){
    $('#generate-contacts-book').modal();
}
var yidongReg = /^139|^138|^137|^136|^135|^134|^159|^150|^151|^158|^157|^188|^187|^152|^182|^147/;
var liantongReg = /^130|^131|^132|^155|^156|^185|^186/;
var dianxinReg = /^133|^153|^180|^189/;
function doGenerateContacts(){
    var cityCode = $('#city option:selected').attr('data-code');
    var operator = $('#operator').val();
    var count = $('#generate-count').val();
    if(cityCode && count.match(/^\d+$/)){
        var sql = "select * from phone_section where city_code = '" + cityCode + "'";
        var res = readDB(sql);
        if(!res.length){
            alert('未找到手机号码，地址区段');
            return;
        }
        var res = formatData(res);
        var data = {data:[], total:0, page:1};
        for(var i=0; i<res.length; i++){
            var record = res[i];
            var operator = '';

            if(record.section.match(yidongReg)){
                operator = '移动';
            }else if(record.section.match(liantongReg)){
                operator = '联通';
            }else if(record.section.match(dianxinReg)){
                operator = '电信';
            }
            data.data.push({id:record.section, section:record.section, operator:operator});
        }
        data.total = res.length;
        qwp.table.update('generate-section', data, 1, null, null, null);
        $('input[name="generate-section"]').change(function(){
            selectPhoneSection($(this).val());
        });
        qwp.table.update('generate-contacts-book', {}, 1, null, null, null);
    }else{
        alert('请选择要生成的城市，填写生成数量！！');
    }
}
function selectPhoneSection(section){
    var count = $('#generate-count').val();
    var phoneData = {data:[], total:0, page:1};
    var phoneArr = [];
    var randomNum;
    for(var i=0; i<count; i++){
        do{
            randomNum = '000' + Math.round(Math.random()*1000);
            randomNum = randomNum.slice(-4);
        }while(phoneArr.indexOf(randomNum) != -1)
        phoneArr.push(randomNum);
        phoneData.data.push({phone: '' + section + randomNum});
    }
    phoneData.total = phoneData.data.length;
    qwp.table.update('generate-contacts-book', phoneData, 1, null, null, null);
}
function doGenerateContactsImport(){
    var data = qwp.table.data('generate-contacts-book');
    var contactsArr = [];
    for(var i=0; i<data.data.length; i++){
        var record = data.data[i];
        contactsArr.push({"name":record.phone, "number":record.phone});
    }
    if(contactsArr.length == 0){
        alert('请生成要导入的联系方式');
        return;
    }
    createTask(deviceOperationType.import_contacts_by_text, contactsArr, true, contactsBook);
}

//APK
function showApkList(){
    var deviceId = $('#device-info input[name="device-id"]').val()
    qwp.get({
        quiet: true,
        url: interfaceUrl + 'api/device_infos/' + deviceId + '/getAppsList',
        params: {},
        fn: function(res){
            var data = {data:[], total:0, page:1};
            var res = res.result;
            for(var i=0; i<res.length; i++){
                var record = res[i];
                data.data.push({
                    name: record.name,
                    version: record.ver,
                    operate: $h.button('删除', {class:'btn btn-success btn-xs', onclick:"removeApk('" + record.pkg + "')"})
                });
            }
            data.total = res.length;
            qwp.table.update('device-apk', data, 1, null, null, null);
            $('#device-apk').modal();
        }
    });
}
function importApk(){
    $('#file_task_type').val(deviceOperationType.install_apk);
    $('#import-file-dialog').modal();
}
function removeApk(pkg){
    createTask(deviceOperationType.uninstall_apk, {'pkg':pkg}, true, showApkList);
}
var map, drawingManager, autocomplete, positionPolygon, rectangleRange;
function initializeGoogleMap(){
    googleMapLoadFlag = true;
    var mapProp = {
        center: new google.maps.LatLng(39.9186693,116.370818),
        zoom: 10,
        mapTypeId: google.maps.MapTypeId.ROADMAP
    };
    map=new google.maps.Map(document.getElementById("google-map"), mapProp);

    drawingManager = new google.maps.drawing.DrawingManager({
        drawingMode: google.maps.drawing.OverlayType.MARKER,
        drawingControl: true,
        drawingControlOptions: {
            position: google.maps.ControlPosition.TOP_RIGHT,
            drawingModes: [
                google.maps.drawing.OverlayType.POLYGON
            ]
        }
    });
    drawingManager.setMap(map);

    drawingManager.addListener('polygoncomplete', function(polygon){
        var posPolygonArr = [];
        var posPolygonStr = '';
        var posArr = polygon.getPaths().getArray();
        posArr = posArr[0].b;
        for(var i=0; i<posArr.length; i++){
            posPolygonArr.push({"lat":posArr[i].lat(), "lng":posArr[i].lng()});
            posPolygonStr += "pos" + (i+1) + "<br/>";
            posPolygonStr += "lat: " + posArr[i].lat() + "<br/>";
            posPolygonStr += "lng: " + posArr[i].lng() + "<br/>";
        }
        $("#position-form #position-polygon-show").html(posPolygonStr);
        $("#position-form #position-polygon").val(JSON.stringify(posPolygonArr));

        if (rectangleRange != null) {
            rectangleRange.setMap(null);
        }
        rectangleRange = polygon;
    });
    autocomplete = new google.maps.places.Autocomplete((document.getElementById('autocomplete-input')), {types:['(cities)']});
    autocomplete.addListener('place_changed', onPlaceChanged);
}
function onPlaceChanged(){
    var place = autocomplete.getPlace();
    if(place.geometry){
        map.panTo(place.geometry.location);
    }else{
        document.getElementById('autocomplete').placeholder = '地址搜索';
    }
}

function showPosition(pos){
    if(!pos.polygon){
        return;
    }
    var polygon = pos.polygon;
    map.setCenter(polygon[0]);
    if (rectangleRange != null) {
        rectangleRange.setMap(null);
    }
    rectangleRange = new google.maps.Polygon({
        paths: polygon
    });
    rectangleRange.setMap(map);

    var posPolygonStr = "";
    for(var i=0; i<polygon.length; i++){
        posPolygonStr += "pos" + (i+1) + "<br/>";
        posPolygonStr += "lat: " + polygon[i].lat + "<br/>";
        posPolygonStr += "lng: " + polygon[i].lng + "<br/>";
    }
    $("#position-form #position-polygon-show").html(posPolygonStr);
    $("#position-form #position-polygon").val(JSON.stringify(polygon));
    $("#position-form #position-name").val(pos.name);
}
//设备实时屏幕， websocket连接
function oneDeviceScreenWS(port){
    var url = 'wss://'+window.location.host+'/screen/1/'+port;//'ws://192.168.57.123:' + port;
    bigScreenSock = new WebSocket(url);
    bigScreenSock.onopen = function(){
        if(bigScreenSock.readyState==1){
            bigScreenSock.send('size 330x1280');
            bigScreenSock.send('on');
        }
    }
    bigScreenSock.onclose = function(){
        bigScreenSock = false;
        console.log('close websocket connection');
    }
    bigScreenSock.onmessage = function(message){
        //console.log(message);
        //eval('var data = '+msg.data);
        //document.getElementById('screen-img').src = data.nrong;
        //$('#device-info .device-info-screen img').attr('src', 'data:image/jpeg;base64,'+data.nrong);
        var blob = new Blob([message.data], {type: 'image/jpeg'});
        var URL = window.URL || window.webkitURL;
        var u = URL.createObjectURL(blob);
        $('#device-info .device-info-screen img').attr('src', u);
        $('#device-info .device-info-screen img').one('load', function() {
            // console.log('revokeObjectURL')
            URL.revokeObjectURL(u)
        })
            
    }
}
$('#device-info').on('hide.bs.modal', function (e) {
    if (bigScreenSock) {
        bigScreenSock.send('size 120x170');
        bigScreenSock.send('on');
        bigScreenSock.close()
    }
})
function operateDeviceWifi(){
    var enabled;
    if($('#set-wifi-btn').attr('action_type') == 'open'){
        enabled = true;
    }else{
        enabled = false;
    }
    var result = createTask(deviceOperationType.wifi, {"enable": enabled});
    if(result){
        if(enabled){
            $('#device-info #set-wifi-btn').text('关闭WIFI').attr('action_type', 'close');
        }else{
            $('#device-info #set-wifi-btn').text('打开WIFI').attr('action_type', 'open');
        }
    }
}
function operateDeviceAirplane(){
    var enabled;
    if($('#set-airplane-btn').attr('action_type') == 'open'){
        enabled = true;
    }else{
        enabled = false;
    }
    var result = createTask(deviceOperationType.airplane_mode, {"enable": enabled});
    if(result){
        if(enabled){
            $('#device-info #set-airplane-btn').text('关闭飞行模式').attr('action_type', 'close');
        }else{
            $('#device-info #set-airplane-btn').text('打开飞行模式').attr('action_type', 'open');
        }
    }
}