qwp.page={
    "validator": {
        "#profile_info": {
            "rules": {
                "filePath": {
                    "required": true,
                    "_msg": "Phone excel file is required"
                }
            },
            "confirmDialog": "qwp_mbox",
            "formParentDialog": "dialog_profile",
            "mbox": {
                "height": 100,
                "title": "Import new profiles confirmation",
                "message": "Are you sure to import those profiles?"
            },
            "actionMessage": "New phone data is being saved, please wait...",
            "actionHandler": "phoneOpsCallback"
        }
    },
    "forms": [],
    "inputRules": {
        "digits": "^\\d+$",
        "letters": "^([a-z]|[A-Z])+$",
        "alphanumeric": "^[\\w|-]+$",
        "alphanumeric_ex": "^[\\w|-|\\.]+$",
        "url": [
            "^(https?|ftp):\\/\\/[^\\s\\/\\$.?#].[^\\s]*$",
            "i"
        ],
        "password": [
            [
                "^(\\w|\\d|@|!)+$",
                "\\d",
                "[a-z]",
                "[A-Z]"
            ],
            [
                "",
                "",
                "i",
                "i"
            ]
        ],
        "email": "^[a-zA-Z0-9.!#$%&'*+\\/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$",
        "number": "^(?:-?\\d+|-?\\d{1,3}(?:,\\d{3})+)?(?:\\.\\d+)?$",
        "ipv4": "^(25[0-5]|2[0-4]\\d|[01]?\\d\\d?)\\.(25[0-5]|2[0-4]\\d|[01]?\\d\\d?)\\.(25[0-5]|2[0-4]\\d|[01]?\\d\\d?)\\.(25[0-5]|2[0-4]\\d|[01]?\\d\\d?)$",
        "ipv6": "^(?:-?\\d+|-?\\d{1,3}(?:,\\d{3})+)?(?:\\.\\d+)?$",
        "datehour": "^\\d\\d\\d\\d-\\d\\d-\\d\\d \\d\\d:\\d\\d$",
        "datetime": "^\\d\\d\\d\\d-\\d\\d-\\d\\d \\d\\d:\\d\\d:\\d\\d$",
        "date": "^\\d\\d\\d\\d-\\d\\d-\\d\\d$",
        "joined_digits": "^\\d+[\\d|,]*$",
        "base64": "^[A-Za-z0-9\\+\\/=]+$"
    },
    "search": [],
    "m": "profile-profiles",
    "p": null,
    "page": "",
    "pageSize": "",
    "sort": "",
    "sortf": ""
};
qwp.components={
    "dialogs": {
        "dialog_profile": {
            "width": 500,
            "height": 280,
            "tmpl": "dialog_profile"
        }
    }
};
jQuery($READY);