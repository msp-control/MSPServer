const electron = require('electron');
const remote = electron.remote;
const dialog = remote.dialog;
const BrowserWindow = remote.BrowserWindow;
const ipcRenderer = electron.ipcRenderer;
var fs = require('fs');
var path = require('path');
var spawn = require('child_process').spawn;
var exec = require('child_process').exec;
const appPath = __dirname;
var processPath = path.dirname(remote.process.resourcesPath);
var CONFIG = {
    db: 'profile.db',
    db_version: 1,
    debug: false
}
function openWin() {
    var wndOption = {
        'zoom-factor':1.0,
        'auto-hide-menu-bar': true,
        'use-content-size': true,
        'show': CONFIG.debug,
        'enable-larger-than-screen': true
    };
    ExecTaskWin = new BrowserWindow(wndOption);
    if(CONFIG.debug) ExecTaskWin.openDevTools();
    ExecTaskWin.setContentSize(800, 600);
    ExecTaskWin.loadURL("");
    ExecTaskWin.webContents.on('ipc-message',function(){
//        if(arguments[1][0] == 'send-success') {
//            var tmp = arguments[1][1];
//            var obj = {
//                "currentAccount": tmp.currentAccount.account,
//                "currentContent": tmp.currentContent,
//                "successNum": tmp.successNum
//            };
//            nav.feedbackData(obj);
//        }
    });
}
function $C(k, d) {
    return CONFIG[k] ? CONFIG[k] : d;
}
function openDialog() {
    var file = dialog.showOpenDialog();
    if (typeof(file) == "undefined") {
        return;
    }
    return file;
}
function replaceSpecialSymbol(str) {
    var tempStr = str.replace('%^', '%25^');
    return tempStr.replace(/%%/g, '%25%');
}
function $param(search) {
    if (!search) return {};
    search = search.substr(1);
    search = replaceSpecialSymbol(search);
    var p = decodeURIComponent(search).split('&'), obj = {};
    for (var i = p.length - 1; i >= 0; --i) {
        var s = p[i].split('=');
        var p1 = s[0].indexOf('[');
        var p2 = s[0].indexOf(']');
        if (p1 !== -1 && p2 !== -1) {
            var k1 = s[0].substr(0, p1), k2 = s[0].substr(p1 + 1, p2 - p1 - 1);
            if (!obj[k1]) obj[k1] = {};
            obj[k1][k2] = s[1];
        } else {
            obj[s[0]] = s[1];
        }
    }
    return obj;
}