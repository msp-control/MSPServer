var SQL = require(path.join(appPath, 'js', 'sql.js'));
function execDB(sql) {
    var filebuffer  = fs.readFileSync(path.join(processPath, 'db', $C('db')));
    var filedb = new SQL.Database(filebuffer);
    filedb.run(sql);
    var data = filedb.export();
    var buffer = new Buffer(data);
    fs.writeFileSync(path.join(processPath, 'db', $C('db')), buffer);
    filedb.close();
}
function readDB(sql) {
    var filebuffer  = fs.readFileSync(path.join(processPath, 'db', $C('db')));
    var filedb = new SQL.Database(filebuffer);
    var data = filedb.exec(sql);
    filedb.close();
    return data;
}