function base64ImgSrc(data, type) {
    if (!type) type = 'jpeg';
    return 'data:image/' + type + ';base64,' + data;
}
function base64Img(data, type) {
    if (!type) type = 'jpeg';
    return $h.img({src:'data:image/' + type + ';base64,' + data});
}
function unifyOutLink(r, f) {
    if (qwp.isString(f)) f = [f];
    for (var i = 0, cnt = f.length; i < cnt; ++i) {
        if (!r[f[i]]) r[f[i]] = '#';
    }
}
function getSiteImage(id) {
    var sites = {
        '1':'sina',
        '5':'sohu',
        '2':'wy',
        '9':'facebook',
        '59': 'google',
        '52': 'yahoo',
        '86': 'wechat'
    };
    return $h.img({src:'img/sites/' + (sites[id] ? sites[id] : 'default') + '.png', mtag:'site'});
}
function getOperationStatusImage(status, opt, noMTag) {
    var imgs = {
        's': ['ok-blue.png',$L('Success')],
        'f': ['error.gif',$L('Failed')],
        'z': ['stop.png',$L('Paused')],
        'p': ['run.gif',$L('Running')]
    };
    var imgOpt = {
        src:'img/' + (imgs[status] ? imgs[status][0] : 'time.png'),title:(imgs[status] ? imgs[status][1] : $L('Waiting'))
    };
    if (!noMTag) imgOpt.mtag = 'tstat';
    if (opt) $.extend(imgOpt, opt);
    return $h.img(imgOpt);
}
function convertBr(c) {
    if (!c) return '';
    return c.replace(/\r\n/g, '<br>').replace(/\r/g, '\\n').replace(/\n/g, '<br>');
}
function createDateTimePicker() {
    $('input[data-rel=datetime]').datetimepicker({
        format: 'yyyy-mm-dd hh:ii',
        autoclose:true
    }).css('cursor', 'pointer').addClass('date-input').attr('placeholder', $L('Select date time'));
    $('input[data-rel=date]').datetimepicker({
        format: 'yyyy-mm-dd',
        autoclose:true
    }).css('cursor', 'pointer').addClass('date-input').attr('placeholder', $L('Select date'));
}
function createDateRangePicker(selector){
    $(selector).daterangepicker({
        timePicker: true,
        timePickerIncrement: 5,
        locale: {
            format: 'YYYY-MM-DD hh:mm'
        }
    }).val('').on('hide.daterangepicker',function(){
        $(this).attr('title',$(this).val());
    }).on('cancel.daterangepicker', function(ev, picker) {
        $(this).val('');
    }).addClass('date-input').attr('placeholder', $L('Select date range'));
}
function getExecError(r) {
    var execError = false;
    if (r.exec_error && r.exec_error.length > 0) {
        try {
            execError = $.parseJSON(r.exec_error);
        } catch (e) {}
        if (execError) {
            if ($.isArray(execError)) {
                if (execError.length > 0) execError = execError[0];
                else execError = false;
            }
            if (execError.data && execError.data.length && execError.data.length > 0) {
                execError = execError.data;
            } else if (execError.t && execError.t.length && execError.t.length > 0) {
                execError = execError.t;
            } else if (execError.s && execError.s.length && execError.s.length > 0) {
                if (r.error_message) execError = r.error_message;
                else execError = execError.s;
            } else if (r.error_message) {
                execError = r.error_message;
            }
        } else {
            execError = r.exec_error;
        }
    } else if (r.error_message){
        execError = r.error_message;
    }
    return $L(execError ? execError : "Unknown error");
}
function getGenderImg(g, attr) {
    if (g === null || $.type(g) == 'undefined') g = '';
    g = g.toString().toLowerCase();
    var opt;
    if (g == '0' || g == 'm' || g == 'male') opt = {src:'img/male.png', title:$L('Male')};
    else if (g == '1' || g == 'f' || g == 'female') opt = {src:'img/female.png', title:$L('Female')};
    else opt = {src:'img/gender.png', title:$L('Gender is unknown')};
    if (attr) $.extend(opt, attr);
    return $h.img(opt);
}
function getTextTag(filter) {
    return $h.b(filter, {'class':'highlight'});
}
function tagText(filter, r, rp, field) {
    if (!filter) {
        if (!field) return r;
        return;
    }
    var ex = new RegExp(filter, "gm");
    if (field) r[field] = r[field].replace(ex, rp);
    else return r.replace(ex, rp);
}
function formalizeFeed(r, siteId) {
    if (siteId == '9') {
        r.content = r.content.replace(new RegExp('https://www.facebook.com/images/emoji.php', 'gmi'), 'img/emoji/fb');
    }
}
function getSiteUrl(site) {
    switch (site) {
        case '9':
            return 'https://www.facebook.com';
        case '10':
            return 'https://twitter.com';
        case '1':
            return 'https://weibo.com';
    }
    return '';
}
function modalBtnClick(id, opt){
    id = "#"+id;
    if (opt.ok)
    {
        var obj = $(id+" [mtag='ok']");
        obj.unbind("click");
        obj.click(opt.ok);
    }
    if (opt.cancel)
    {
        var obj = $(id+" [mtag='cancel']");
        obj.unbind("click");
        obj.click(opt.cancel);
    }
}