var lifecycle = require('../../util/lifecycle')
var logger = require('../../util/logger')

module.exports = function(options) {
    var log = logger.createLogger('api')

    var app = require ('./server/server.js')
    app.options = options
    app.datasources['mysql'].autoupdate(null);
    app.start();
    
    lifecycle.observe( async function() {
        var db = app.datasources['mysql'].models
        log.info('Close all devices and reset all devices status.')
        try {
            await db.device_info.updateAll({groupInfoId: require('../../../package.json').groupId}
              , {status: 0, appium_port: 0, screen_port: 0})
        } catch (err) {
            log.error(err)
        }
    })
}
