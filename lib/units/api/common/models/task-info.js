'use strict';
var Promise = require('bluebird')
var moment = require('moment')

var zmqutil = require('../../../../util/zmqutil')
var pathutil = require('../../../../util/pathutil')

module.exports = function(Taskinfo) {
    Taskinfo.createTask = async function(type, devs, args, cb){
        var app = require('../../server/server')
          , Deviceinfo = app.datasources['mysql'].models.device_info 
          , Tasktype = app.datasources['mysql'].models.task_type 
          , Taskdetail = app.datasources['mysql'].models.task_detail

        var task_ids = []
          , tasks = []
          , success = true
          , error
          , channels = devs

        var push = zmqutil.socket('push')
        push.connect(app.options.endpoints.push)

        async function checkDevs() {
            var errids = []
            if ('undefined' !== typeof devs && devs.length !== 0) {
                for(var id of devs) {
                    var model = await Deviceinfo.findById(id, {fields: ['id']})
                    if (!model) {
                        errids.push(id)
                    }   
                }  
            }
            return errids
        }

        try {
            if ('undefined' === typeof devs || devs.length === 0) {
                console.log('devs.length is 0.')
                channels = []
                var model = await Deviceinfo.find({
                  fields: ['id']
                , where: {
                    groupInfoId: require(pathutil.root('package.json')).groupId
                  , status:{gt: 0}
                  }
                })
                for (var m of model) {
                    channels.push(m.id)
                }
            }

            while (true) {
                var errids = await checkDevs()
                if ('undefined' !== typeof errids && errids.length !== 0) {
                    success = false
                    error = 'Can not find device which id is ' + errids.toString()
                    break
                }

                var model = await Tasktype.findById(type, {fields: ['taskClassId']})
                if (!model) {
                    success = false
                    error = 'Can not find this type of task.'
                    break
                }

                // request task
                if (model.taskClassId === 1){
                    var td_model = await Taskdetail.upsert({type: type, args: args})
                }

                for( var id of channels) {
                    var taskid = 0
                    if (model.taskClassId === 1){
                        var ti_model = await Taskinfo.upsert(
                              {start_time:moment().format('YYYY-MM-DD HH:mm:ss')
                            , device_id: id
                            , taskDetailId: td_model.id})
                                
                        taskid = ti_model.id
                        task_ids.push(taskid)
                    }

                    var dev_model = await Deviceinfo.findById(id, {fields: ['adb_serial']})
                    if (!dev_model || !dev_model.adb_serial) {
                        success = false
                        error = "Error find adb_serial by id " + id + "from device_info."
                        break
                    }
                    tasks.push({channel: dev_model.adb_serial, message: {devid: id, id: taskid, type: type, args: args}})
                }
                if (!success) {
                    break
                }
                break
            }
        } catch (err) {
            success = false
            error = err.toString()
        }

        if (success) {
            // push task here
            for(var task of tasks) {
                console.log('Send task to', task.channel)
                push.send([task.channel, JSON.stringify(task.message)])
            }

            cb(null, true, task_ids)
        } else {
            cb(null, false, null, error)
        }
        push.close()
    }

    Taskinfo.remoteMethod(
        'createTask',
        {
            http: {path: '/createTask', verb: 'post'},
            accepts: [{arg: 'type', type: 'number'},
                      {arg: 'devs', type: 'array'},
                      {arg: 'args', type: 'object'}],
            returns: [{arg: 'success', type: 'boolean'},
                      {arg: 'id', type: 'object'},
                      {arg: 'error', type: 'object'}]
        }
    );
};
