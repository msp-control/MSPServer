'use strict';
var util = require('util')

module.exports = function(Tools) {
    var model_tac = [
        {model: 'm3 note', tac: '86305803'}
      , {model: 'Lenovo K30-T', tac: '86779202'}
      , {model: 'Redmi 3', tac: '86905502'}
    ]

    const getSerial = () => {
        var serial = ''
          , i = 0
        while(i < 8) {
            serial += Math.floor(Math.random() * 0xF).toString(16)
            i++
        }
        return serial
    }

    const getModel = () => {
        return model_tac[Math.floor(Math.random() * model_tac.length)].model
    }

    const getImei = (model) => {
        var tac = '00000000'
        for (var i = 0; i < model_tac.length; i++) {
            if (model === model_tac[i].model) {
                tac = model_tac[i].tac
                break
            }
        }

        var num = Math.floor(Math.random() * 1000000)
        var snr = pad(num, 6)
        return tac + snr + luhn(tac+snr)
    }

    const getImsi = () => {
        var title = '46000'
        var second = 0
        do {
            second = Math.floor(Math.random() * 8)
        } while (second === 4)

        var r1 = pad(Math.floor(Math.random() * 100000), 5)
        var r2 = pad(Math.floor(Math.random() * 100000), 5)

        return title + second + r1 + r2
    }

    const getMac = () => {
        var mac = ''
        for ( var i = 0; i < 6; ++i) {
            var t = Math.floor(Math.random() * 0xFF).toString(16)
            if ( t.length === 1) {
                t = '0' + t
            }
            mac += t
            if ( i === 5){
                break
            }
            mac += '-'
        }

        return mac
    }

    // Number to string format n chars.
    function pad(num, n) {  
        var len = num.toString().length;  
        while(len < n) {  
            num = "0" + num;  
            len++;  
        }  
        return num;  
    }

    // Check algorithm
    const luhn = (str) => {
        var sum = 0
        var tmp
        for (var i = str.length - 1, j = 0; i >= 0; i--, j++){
            tmp = Number(str[i])
            if (j%2 == 0){
                sum += parseInt(tmp*2/10 + tmp*2%10)
            } else {
                sum += tmp
            }
        }
        if (sum % 10 == 0){
            return 0;
        }
        return 10 - sum%10;
    }

    Tools.genVirDevInfo = (cb) => {
        var serial = getSerial()
        var model = getModel()
        var imei = getImei(model)
        var imsi = getImsi()
        var mac = getMac()

        cb(null, {'serial': serial, 
                    'model': model,
                    'imei': imei,
                    'imsi': imsi,
                    'mac': mac});
    }

    Tools.remoteMethod(
        'genVirDevInfo',
        {   
            http: {path: '/genVirDevInfo', verb: 'get'},
            returns: {arg: 'genInfo', type: 'object'}
        }
    );

};
