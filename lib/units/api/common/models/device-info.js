'use strict';
var zmqutil = require('../../../../util/zmqutil')

module.exports = function(Deviceinfo) {
    async function getCommon(id, type, args, cb) {
        var app = require('../../server/server')
          , Deviceinfo = app.datasources['mysql'].models.device_info

        var model = await Deviceinfo.findById(id, {fields: ['id', 'adb_serial']})
        if (!model) {
            cb(null, false, 'Can not find device which id is ' + id)
            return
        }

        var task = {}
        var taskId = id*10000 + type
        task.message = {id: taskId, type: type, args: args}
        
        var sub = zmqutil.socket('sub')
        sub.connect(app.options.endpoints.sub)
        var push = zmqutil.socket('push')
        push.connect(app.options.endpoints.push)
        push.send([model.adb_serial, JSON.stringify(task.message)])

        sub.subscribe(taskId.toString())
        sub.on('message', (channel, message) => {
            sub.close()
            push.close()
            cb(null, true, JSON.parse(message).ret_msg)
        })
    }

    Deviceinfo.getAppsList = function(id, cb){
        getCommon(id, 1018, null, cb)
    }

    Deviceinfo.getContactsList = function(id, cb){
        getCommon(id, 1019, null, cb)
    }

    Deviceinfo.getPicturesList = function(id, cb){
        getCommon(id, 1027, null, cb)
    }

    Deviceinfo.execSuCmd = function(id, cmd, cb){
        getCommon(id, 1028, {cmd:cmd}, cb)
    }

    Deviceinfo.remoteMethod(
        'getAppsList',
        {
            http: {path: '/:id/getAppsList', verb: 'get'},
            accepts: [{arg: 'id', type: 'number', required: true}],
            returns: [{arg: 'success', type: 'boolean'},
                      {arg: 'result', type: 'object'}]
        }
    );

    Deviceinfo.remoteMethod(
        'getContactsList',
        {
            http: {path: '/:id/getContactsList', verb: 'get'},
            accepts: [{arg: 'id', type: 'number', required: true}],
            returns: [{arg: 'success', type: 'boolean'},
                      {arg: 'result', type: 'object'}]
        }
    );

    Deviceinfo.remoteMethod(
        'getPicturesList',
        {
            http: {path: '/:id/getPicturesList', verb: 'get'},
            accepts: [{arg: 'id', type: 'number', required: true}],
            returns: [{arg: 'success', type: 'boolean'},
                      {arg: 'result', type: 'object'}]
        }
    );

    Deviceinfo.remoteMethod(
        'execSuCmd',
        {   
            http: {path: '/:id/execSuCmd', verb: 'post'},
            accepts: [{arg: 'id', type: 'number', required: true},
                      {arg: 'cmd', type: 'string'}],
            returns: [{arg: 'success', type: 'boolean'},
                      {arg: 'result', type: 'object'}]
        }
    );
};
