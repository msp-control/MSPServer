var Promise = require('bluebird')
var moment = require('moment')

var logger = require('../../util/logger')
var lifecycle = require('../../util/lifecycle')
var zmqutil = require('../../util/zmqutil')
var db = require('../../util/dbutil')
var PROTO = require('../../util/proto')

module.exports = function(options) {
  var log = logger.createLogger('processor')

  // App side
  var appDealer = zmqutil.socket('dealer')
  appDealer.connect(options.endpoints.appDealer)

  // Device side
  var devDealer = zmqutil.socket('dealer')
  devDealer.connect(options.endpoints.devDealer)

  appDealer.on('message', function(channel, data) {
    log.info(channel.toString(), data.toString())
    devDealer.send([channel, data])
  })

  devDealer.on('message', function(channel, data) {
    // log.info(channel.toString(), data.toString())
    var envelope = JSON.parse(data)
    
    // log.info(channel.toString(), data.toString())

    switch(envelope.type) {
      case PROTO.getAppsList.id:
      case PROTO.getContactsList.id:
      case PROTO.getPicturesList.id:
      case PROTO.suShell.id:
        appDealer.send([envelope.id.toString(), data.toString()])
        break
      case 1: // Heartbeat
        var envelope = envelope.ret_msg
        // log.info(channel.toString(), data.toString())
        db.device_info.upsertWithWhere({identity_id: envelope.identityId}
        , {
            identity_id: envelope.identityId,
            serial: envelope.serial,
            abi: envelope.abi,
            iccid: envelope.iccid,
            imei: envelope.imei,
            imsi: envelope.imsi,
            mac: envelope.mac,
            manufacturer: envelope.manufacturer,
            model: envelope.model,
            platform: envelope.platform,
            product: envelope.product,
            sdk: envelope.sdk,
            version: envelope.version,
            android_id: envelope.androidId,
            network: envelope.network,
            cpu_percent: envelope.cpuPercent,
            memory_percent: envelope.memPercent,
            battery_percent: envelope.battery,
            signal_strength: envelope.signalStrength,
            in_ip: envelope.inIp,
            out_ip: envelope.outIp,
            adb_serial: envelope.adbSerial,
            status: envelope.status,
            wifi_status: envelope.wifiState,
            airplane_mode_status: envelope.airplaneModeStatus,
            brightness: envelope.brightness,
            screen_timeout: envelope.screenTimeout,
            screen_port: envelope.screenPort,
            appium_port: envelope.appiumPort,
            groupInfoId: require('../../../package.json').groupId
          }
        , (err, obj) => {
          if (err) {
            log.error(err)
          }
        })
        break;
      default:
        if ( typeof envelope.id !== "undefined"
          && envelope.id !== null 
          && envelope.id !== 0) {
            db.task_info.upsert(
              {end_time:moment().format('YYYY-MM-DD HH:mm:ss')
            , id: envelope.id
            , ret_code: envelope.ret_code
            , ret_msg: envelope.ret_msg})
        }
    }
  })

  lifecycle.observe(function() {
    [appDealer, devDealer].forEach(function(sock) {
      try {
        sock.close()
      }
      catch (err) {
        // No-op
      }
    })
  })
}
