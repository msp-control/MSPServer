var http = require('http')
var events = require('events')

var WebSocket = require('ws')
var Promise = require('bluebird')
var _ = require('lodash')
var adbkit = require('adbkit')

var logger = require('../../util/logger')
var wirerouter = require('../../wire/router')
var lifecycle = require('../../util/lifecycle')
var zmqutil = require('../../util/zmqutil')
var pathutil = require('../../util/pathutil')

module.exports = function(options) {
  var log = logger.createLogger('websocket')
//  var server = http.createServer()
//  var io = socketio.listen(server, {
//        serveClient: false
//      , transports: ['websocket']
//      })

  var wss = new WebSocket.Server({
    port: options.port 
  , perMessageDeflate: false
  })

  var channelRouter = new events.EventEmitter()
  var channels = []
  var messageListener = wirerouter()

  // Output
  var push = zmqutil.socket('push')
  push.connect(options.endpoints.push)

  // Input
  var sub = zmqutil.socket('sub')
  sub.connect(options.endpoints.sub)

  sub.on('message', function(channel, data) {
    channelRouter.emit(channel.toString(), channel, data)
  })

  function joinChannel(channel) {
    channels.push(channel)
    channelRouter.on(channel, messageListener)
    sub.subscribe(channel)
  }

  function leaveChannel(channel) {
    _.pull(channels, channel)
    channelRouter.removeListener(channel, messageListener)
    sub.unsubscribe(channel)
  }

  const getTasks = async (data) => {
    var tasks = []
    var js_data = JSON.parse(data)
    var db = require('../../util/dbutil')
    var dev_ids = js_data.devs
    var adbserials = []
    if (typeof dev_ids === 'undefined' || dev_ids.length === 0) {
      dev_ids = []
      var model = await db.device_info.find({
        fields: ['id']
      , where: {groupInfoId: require(pathutil.root('package.json')).groupId, status:{gt: 0}}
      })
      for (var m of model) {
        dev_ids.push(m.id)
      }
    }

    for (var id of dev_ids) {
      var dev_model = await db.device_info.findById(id, {fields: ['adb_serial']})
      if (!dev_model || !dev_model.adb_serial) {
        continue
      }
      tasks.push({
        channel: dev_model.adb_serial
      , message: {
          devid: id
        , id: 0
        , type: js_data.type
        , args: js_data.args
        }
      })
    }
    return tasks
  }


  wss.on('connection', function(socket) {
    // messageListener.on('xxx', function(channel, message) {
    //     socket.emit('xxx', message)
    //   })
    //   .handler()
    log.info('New client connected!')
    // socket.on('disconnect', resolve)
    // Touch events
    // for (var e of ['2000','2001','2002','2003','2004','2005','2006','2007']) {
      socket.on('message', async function(data) {
        log.info(data)
        var tasks = await getTasks(data)
        log.info(JSON.stringify(tasks))
        for(var task of tasks) {
          push.send([task.channel, JSON.stringify(task.message)])
        }
      })
    //}
  })    

  lifecycle.observe(function() {
    channels.forEach(function(channel) {
      channelRouter.removeListener(channel, messageListener)
      sub.unsubscribe(channel)
    })

    [push, sub].forEach(function(sock) {
      try {
        sock.close()
      }
      catch (err) {
        // No-op
      }
    })
  })

//  server.listen(options.port)
  log.info('Listening on port %d', options.port)
}
