var adb = require('adbkit')
var Promise = require('bluebird')
var childprocess = require('child_process')

var _ = require('lodash')
var EventEmitter = require('eventemitter3')
var lifecycle = require('../../util/lifecycle')
var logger = require('../../util/logger')
var procutil = require('../../util/procutil')
var pathutil = require('../../util/pathutil')

module.exports = function (options) {
  var log = logger.createLogger('provider')
  var client = adb.createClient({
    host: options.adbHost
    , port: options.adbPort
  })
  var workers = {}
  var appiums = {}
  var lists = {
    all: []
    , ready: []
    , waiting: []
  }
  var totalsTimer

  // To make sure that we always bind the same type of service to the same
  // port, we must ensure that we allocate ports in fixed groups.
  var ports = options.ports.slice(
    0
    , options.ports.length - options.ports.length % 4
  )

  // Information about total devices
  var delayedTotals = (function () {
    function totals() {
      if (lists.waiting.length) {
        log.info(
          'Providing %d of %d device(s); waiting for "%s"'
          , lists.ready.length
          , lists.all.length
          , lists.waiting.join('", "')
        )

        delayedTotals()
      }
      else if (lists.ready.length < lists.all.length) {
        log.info(
          'Providing all %d of %d device(s); ignoring "%s"'
          , lists.ready.length
          , lists.all.length
          , _.difference(lists.all, lists.ready).join('", "')
        )
      }
      else {
        log.info(
          'Providing all %d device(s)'
          , lists.all.length
        )
      }
    }

    return function () {
      clearTimeout(totalsTimer)
      totalsTimer = setTimeout(totals, 10000)
    }
  })()


  // Track and manage devices
  client.trackDevices().then(function (tracker) {
    log.info('Tracking devices')

    // This can happen when ADB doesn't have a good connection to
    // the device
    function isWeirdUnusableDevice(device) {
      return device.id === '????????????'
    }

    // Check whether the device is remote (i.e. if we're connecting to
    // an IP address (or hostname) and port pair).
    function isRemoteDevice(device) {
      return device.id.indexOf(':') !== -1
    }

    // Helper for ignoring unwanted devices
    function filterDevice(listener) {
      return function (device) {
        if (isWeirdUnusableDevice(device)) {
          log.warn('ADB lists a weird device: "%s"', device.id)
          return false
        }
        if (!options.allowRemote && isRemoteDevice(device)) {
          log.info(
            'Filtered out remote device "%s", use --allow-remote to override'
            , device.id
          )
          return false
        }
        return listener(device)
      }
    }

    // To make things easier, we're going to cheat a little, and make all
    // device events go to their own EventEmitters. This way we can keep all
    // device data in the same scope.
    var flippedTracker = new EventEmitter()

    tracker.on('add', filterDevice(function (device) {
      log.info('Found device "%s" (%s)', device.id, device.type)

      var privateTracker = new EventEmitter()
      var willStop = false
      var timer
      var deviceWorker, deviceProc
      var appiumWorker, appiumProc
      var appiumPort = ports.shift()
      var bootstrapPort = ports.shift()

      // Spawn a device worker
      function spawnDevice() {
        var allocatedPorts = ports.splice(0, 4)
        deviceProc = options.fork(device, allocatedPorts.slice(), appiumPort)
        var resolver = Promise.defer()

        function exitListener(code, signal) {
          if (signal) {
            log.warn(
              'Device worker "%s" was killed with signal %s, assuming ' +
              'deliberate action and not restarting'
              , device.id
              , signal
            )
            resolver.resolve()
          }
          else if (code === 0) {
            log.info('Device worker "%s" stopped cleanly', device.id)
            resolver.resolve()
          }
          else {
            resolver.reject(new procutil.ExitError(code))
          }
        }

        function errorListener(err) {
          log.error(
            'Device worker "%s" had an error: %s'
            , device.id
            , err.message
          )
        }

        function messageListener(message) {
          switch (message) {
            case 'ready':
              _.pull(lists.waiting, device.id)
              lists.ready.push(device.id)
              break
            default:
              log.warn(
                'Unknown message from device worker "%s": "%s"'
                , device.id
                , message
              )
              break
          }
        }

        deviceProc.on('exit', exitListener)
        deviceProc.on('error', errorListener)
        deviceProc.on('message', messageListener)

        lists.waiting.push(device.id)

        return resolver.promise
          .finally(function () {
            log.info('Cleaning up device worker "%s"', device.id)

            deviceProc.removeListener('exit', exitListener)
            deviceProc.removeListener('error', errorListener)
            deviceProc.removeListener('message', messageListener)

            // Return used ports to the main pool
            Array.prototype.push.apply(ports, allocatedPorts)

            // Update lists
            _.pull(lists.ready, device.id)
            _.pull(lists.waiting, device.id)
          })
          // .catch(Promise.CancellationError, function() {
          //   log.info('Gracefully killing device worker "%s"', device.id)
          //   return procutil.gracefullyKill(deviceProc, options.killTimeout)
          // })
          .catch(Promise.TimeoutError, function (err) {
            log.error(
              'Device worker "%s" did not stop in time: %s'
              , device.id
              , err.message
            )
          })
      }

      // Spawn a appium worker.
      function spawnAppium() {
        var resolver = Promise.defer()
        var cli = pathutil.module('appium')
        var args = [
          '-p', appiumPort,
          '-bp', bootstrapPort,
          '--session-override',
          '-U', device.id,
          "--no-reset"
          //   "--dont-stop-app-on-reset"
        ]

        appiumProc = childprocess.fork(cli, args)

        function exitListener(code, signal) {
          if (signal) {
            log.warn(
              'Appium worker "%s" was killed with signal %s.'
              , device.id
              , signal
            )
            resolver.resolve()
          }
          else if (code === 0) {
            log.info('Appium worker "%s" stopped cleanly', device.id)
            resolver.resolve()
          }
          else {
            resolver.reject(new procutil.ExitError(code))
          }
        }

        function errorListener(err) {
          log.error(
            'Appium worker "%s" had an error: %s'
            , device.id
            , err.message
          )
        }

        appiumProc.on('exit', exitListener)
        appiumProc.on('error', errorListener)

        return resolver.promise
          .finally(function () {
            log.info('Cleaning up appium worker "%s"', device.id)

            appiumProc.removeListener('exit', exitListener)
            appiumProc.removeListener('error', errorListener)

            ports.push(appiumPort)
            ports.push(bootstrapPort)
          })
          .catch(Promise.TimeoutError, function (err) {
            log.error(
              'Appium worker "%s" did not stop in time: %s'
              , device.id
              , err.message
            )
          })
      }

      // Starts a appium worker.
      function startAppium() {
        return (appiumWorker = appiums[device.id] = spawnAppium())
          .then(function () {
            log.info('Appium worker "%s" has retired', device.id)
            delete appiums[device.id]
            appiumWorker = null
          })
          .catch(procutil.ExitError, function (err) {
            if (!willStop) {
              log.error(
                'Appium worker "%s" died with code %s'
                , device.id
                , err.code
              )
              log.info('Restarting appium worker "%s"', device.id)
              return Promise.delay(5000)
                .then(function () {
                  return startAppium()
                })
            }
          })
      }

      // Starts a device worker.
      function startDevice() {
        return (deviceWorker = workers[device.id] = spawnDevice())
          .then(function () {
            log.info('Device worker "%s" has retired', device.id)
            delete workers[device.id]
            deviceWorker = null
          })
          .catch(procutil.ExitError, function (err) {
            if (!willStop) {
              log.error(
                'Device worker "%s" died with code %s'
                , device.id
                , err.code
              )
              log.info('Restarting device worker "%s"', device.id)
              return Promise.delay(5000)
                .then(function () {
                  return startDevice()
                })
            }
          })
      }

      // Starts a device worker and keeps it alive
      function work() {
        startAppium()
        startDevice()
      }

      // No more work required
      function stop() {
        if (deviceProc) {
          log.info('Shutting down device worker "%s"', device.id)
          log.info('Gracefully killing device worker "%s"', device.id)
          procutil.gracefullyKill(deviceProc, options.killTimeout)
        }

        if (appiumProc) {
          log.info('Shutting down appium worker "%s"', device.id)
          log.info('Gracefully killing appium worker "%s"', device.id)
          procutil.gracefullyKill(appiumProc, options.killTimeout)
        }

        doDeviceOffline()
      }

      // Check if we can do anything with the device
      function check() {
        clearTimeout(timer)

        if (device.present) {
          // We might get multiple status updates in rapid succession,
          // so let's wait for a while
          switch (device.type) {
            case 'device':
            case 'emulator':
              willStop = false
              timer = setTimeout(work, 100)
              break
            default:
              willStop = true
              timer = setTimeout(stop, 100)
              break
          }
        }
        else {
          willStop = true
          stop()
        }
      }

      // Will be set to false when the device is removed
      _.assign(device, {
        present: true
      })
      check()

      // Statistics
      lists.all.push(device.id)
      delayedTotals()

      // Write status to db when device offline.
      async function doDeviceOffline() {
        var db = require('../../util/dbutil')
        log.info(device.id + ' offline')
        try {
          var model = await db.device_info.updateAll({ adb_serial: device.id }
            , { status: 0, appium_port: 0, screen_port: 0 })
          if (typeof model === 'undefined') {
            log.error(err)
          }
        } catch (err) {
          log.error(err.message)
        }
      }

      // When any event occurs on the added device
      function deviceListener(type, updatedDevice) {
        // Okay, this is a bit unnecessary but it allows us to get rid of an
        // ugly switch statement and return to the original style.
        privateTracker.emit(type, updatedDevice)
      }

      // When the added device changes
      function changeListener(updatedDevice) {
        log.info(
          'Device "%s" is now "%s" (was "%s")'
          , device.id
          , updatedDevice.type
          , device.type
        )

        _.assign(device, {
          type: updatedDevice.type
        })

        check()
      }

      // When the added device gets removed
      function removeListener() {
        log.info('Lost device "%s" (%s)', device.id, device.type)

        clearTimeout(timer)
        flippedTracker.removeListener(device.id, deviceListener)
        _.pull(lists.all, device.id)
        delayedTotals()

        _.assign(device, {
          present: false
        })

        check()
      }

      flippedTracker.on(device.id, deviceListener)
      privateTracker.on('change', changeListener)
      privateTracker.on('remove', removeListener)
    }))

    tracker.on('change', filterDevice(function (device) {
      flippedTracker.emit(device.id, 'change', device)
    }))

    tracker.on('remove', filterDevice(function (device) {
      flippedTracker.emit(device.id, 'remove', device)
    }))

  })

  lifecycle.observe(function () {
    clearTimeout(totalsTimer)

    return Promise.all(Object.keys(workers).map(function (serial) {
      workers[serial].cancel()
      appiums[serial].cancel()
    }))
  })
}