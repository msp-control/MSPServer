var syrup = require('stf-syrup')
var zmqutil = require('../../../util/zmqutil')

module.exports = syrup.serial()
  .define(function(options) {
    // Output
    var push = zmqutil.socket('push')
    push.connect(options.endpoints.push)
    return push
  })
