var syrup = require('stf-syrup')
var adbkit = require('adbkit')

var logger = require('../../../util/logger')
var Promise = require('bluebird')

module.exports = syrup.serial()
  .define(function(options) {
    var log = logger.createLogger('device:support:adb')
    var adb = adbkit.createClient({
      host: options.adbHost
    , port: options.adbPort
    })
    adb.Keycode = adbkit.Keycode

    function ensureBootComplete() {
      return new Promise( (resolve, reject) => {
        var timer = setInterval(() => {
          log.info('Waiting for boot to complete')}
          , 1000)
        adb.waitBootComplete(options.serial)
          .timeout(options.bootCompleteTimeout)
          .catch((err) => {log.error(err)})
          .finally(() => {resolve(); clearInterval(timer)})
      })
    }


    return ensureBootComplete()
      .return(adb)
  })
