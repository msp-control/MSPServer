var syrup = require('stf-syrup')
var zmqutil = require('../../../util/zmqutil')

module.exports = syrup.serial()
  .define(function(options) {
    // Input
    var sub = zmqutil.socket('sub')
    sub.connect(options.endpoints.sub)
    sub.subscribe(options.serial)
    sub.subscribe('*ALL')
    return sub
  })
