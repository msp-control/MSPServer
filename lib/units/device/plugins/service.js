var util = require('util')
var events = require('events')

var Promise = require('bluebird')
var syrup = require('stf-syrup')

var devutil = require('../../../util/devutil')
var wireutil = require('../../../wire/util')
var streamutil = require('../../../util/streamutil')
var pathutil = require('../../../util/pathutil')
var logger = require('../../../util/logger')
var lifecycle = require('../../../util/lifecycle')
var db = require('../../../util/dbutil')
var PROTO = require('../../../util/proto')

var heartbeatTimer

function MessageResolver() {
  this.resolvers = Object.create(null)

  this.await = function(id, resolver) {
    this.resolvers[id] = resolver
    return resolver.promise
  }

  this.resolve = function(id, value) {
    var resolver = this.resolvers[id]
    delete this.resolvers[id]
    resolver.resolve(value)
    return resolver.promise
  }
}

module.exports = syrup.serial()
  .dependency(require('../support/adb'))
  .dependency(require('../support/router'))
  .dependency(require('../support/push'))
  .dependency(require('../resources/service'))
  .dependency(require('../resources/hook'))
  .define(function(options, adb, router, push, apk) {
    var log = logger.createLogger('device:plugins:service')
    var messageResolver = new MessageResolver()
    var plugin = new events.EventEmitter()

    var service = {
      socket: null
    , sock: 31218 //'localabstract:mspservice'
    }

    function callService(intent) {
      log.info(util.format(
        'am startservice --user 0 %s'
        , intent
        ))
      return adb.shell(options.serial, util.format(
        'am startservice --user 0 %s'
        , intent
        ))
        .timeout(15000)
        .then(function(out) {
          return streamutil.findLine(out, /^Error/)
            .finally(function() {
              out.end()
            })
            .timeout(10000)
            .then(function(line) {
              if (line.indexOf('--user') !== -1) {
                return adb.shell(options.serial, util.format(
                  'am startservice %s'
                  , intent
                  ))
                  .timeout(15000)
                  .then(function() {
                    return streamutil.findLine(out, /^Error/)
                      .finally(function() {
                        out.end()
                      })
                      .timeout(10000)
                      .then(function(line) {
                        throw new Error(util.format(
                          'Service had an error: "%s"'
                          , line
                        ))
                      })
                      .catch(streamutil.NoSuchLineError, function() {
                        return true
                      })
                  })
              }
              else {
                throw new Error(util.format(
                  'Service had an error: "%s"'
                  , line
                ))
              }
            })
            .catch(streamutil.NoSuchLineError, function() {
              return true
            })
            .catch(Error, function(err) {
              log.error(err)
              lifecycle.fatal()
            })
        })
    }

    function prepareForServiceDeath(conn) {
      function endListener() {
        var startTime = Date.now()
        log.important('Service connection ended, attempting to relaunch')

        /* eslint no-use-before-define: 0 */
        openService()
          .timeout(15000)
          .then(function() {
            log.important('Service relaunched in %dms', Date.now() - startTime)
            if (heartbeatTimer) {
              clearTimeout(heartbeatTimer)
            } 
            heartbeatTimer = setTimeout(heartbeatTimeout, 10000)
          })
          .catch(function(err) {
            log.fatal('Service connection could not be relaunched', err.message)
            lifecycle.fatal()
          })
      }

      conn.once('end', endListener)

      conn.on('error', function(err) {
        log.fatal('Service connection had an error', err.stack)
        lifecycle.fatal()
      })
    }

    // First 4 bytes is length, others are json content.
    var leftLength = -1
    var buffData = ''
    function handleEnvelope(data) {
      // log.info(data)
      var needOffset = false
      if (leftLength === -1) {
        buffData = ''
        var len = data.readUIntBE(0, 4)
        // log.info('bbbbbbbbbbbbbbbbbbbbbbbbbbbbbb   '+len)
        leftLength = len + 4 - data.length
        needOffset = true
      }

      if (needOffset) {
        buffData += data.toString('utf-8', 4, data.length)
      } else {
        buffData += data.toString('utf-8', 0, data.length)
        leftLength -= data.length
      }

      if (leftLength === 0) {
        leftLength = -1
        var envelope = JSON.parse(buffData)
        // log.info(buffData)
        if (typeof envelope.id !== 'undefined') {
          messageResolver.resolve(envelope.id, buffData)
        } else {
          switch(envelope.type) {
            // case 'heartbeat':
            //   // envelope.msg.adbSerial = options.serial
            //   // envelope.msg.screenPort = options.screenPort
            //   push.send([
            //     options.serial
            //   , JSON.stringify(envelope)
            //   ])
          }
        }
      }
    }

    // The APK should be up to date at this point. If it was reinstalled, the
    // service should have been automatically stopped while it was happening.
    // So, we should be good to go.
    function openService() {
      log.info('Launching service')
      return callService(util.format(
        "-a '%s' -n '%s'"
        , apk.startIntent.action
        , apk.startIntent.component
        ))
        .then(function() {
          log.info('Started service.')
          
          return devutil.waitForTcpSocket(adb, options.serial, service.sock)
            .timeout(1500)
            .then(function(conn) {
              log.info("Connected to service!")
              service.socket = conn
              service.socket.on('data', handleEnvelope)
              return prepareForServiceDeath(conn)
            })
            .catch((err) => {
              return openService()
            })
        })
    }

    function runServiceCommand(message) {
      var resolver = Promise.defer()
      try {
        service.socket.write(JSON.stringify(message))
      } catch (err) {
        log.error(err.message)
        lifecycle.fatal()
      }
      
      return messageResolver.await(message.id, resolver)
    }

    function killService() {
      log.info('Killing mspservice.')
      adb.shell(options.serial, util.format(
        "am force-stop '%s'"
      , apk.pkg
      ))
      .timeout(10000)
      .then(() => {
        if (service.socket) {
          service.socket.end()
        }
      })
      .catch((err) => {
        log.error('Kill service failed.', err.message)
        lifecycle.fatal()
      })
    }

    // function killService() {
    //   log.info('Killing mspservice.')
    //   adb.shell(options.serial, util.format(
    //     "am broadcast -a '%s'"
    //   , apk.broadcast.kill
    //   ))
    //   .timeout(10000)
    //   .then(() => {
    //     return adb.shell(options.serial, util.format(
    //       "am stopservice --user 0 -n '%s'"
    //     , apk.startIntent.component
    //     )).timeout(10000)
    //   })
    //   .then(() => {
    //     return adb.shell(options.serial, 'netstat|grep 31218')
    //     .timeout(10000)
    //     .then(require('adbkit').util.readAll)
    //     .then((out) => {
    //       var outStr = out.toString().replace(/\r\n/g, '')
    //       outStr = outStr.replace(/\n/g, '')
    //       log.info(outStr.toString())
    //       if (outStr.indexOf('LISTEN')) {
    //         killService()
    //       } else {
    //         if (service.socket) {
    //           service.socket.end()
    //         }
    //       }
    //     })
    //   })
    //   .catch((err) => {
    //     log.error('Kill service failed.', err.message)
    //     lifecycle.fatal()
    //   })
    // }

    function heartbeatTimeout() {
      var message = {
        'id': -1
      , 'type': 1
      }
      return runServiceCommand(message)
      .timeout(5000)
      .then(data => {
        var envelope = JSON.parse(data)
        envelope.ret_msg.screenPort = options.screenPort
        envelope.ret_msg.adbSerial = options.serial
        envelope.ret_msg.appiumPort = options.appiumPort
        push.send([
          options.serial
        , JSON.stringify(envelope)
        ])
        clearTimeout(heartbeatTimer)
        heartbeatTimer = setTimeout(heartbeatTimeout, 10000)
      })
      .catch((err) => {
        log.error("(heartbeatTimeout)Receive from service timeout, ready kill mspservice.", err.message)
        clearTimeout(heartbeatTimer)
        killService()
      })
    }

    return adb.shell(options.serial, util.format(
        "am broadcast -a '%s'"
      , apk.broadcast.id
      ))
      .timeout(10000)
      .then(require('adbkit').util.readAll)
      .then((output) => {
        var outStr = output.toString().replace(/\r\n/g, '')
        outStr = outStr.replace(/\n/g, '')
        var info = outStr.match(/.*result=0, data="(.*)"$/)
        return new Promise(function(resolve, reject) {
          if (info == null) {
            resolve("")
          } else {
            resolve(info[1])
          }
        })
      })
      .then(function(id) {
        if (id !== "") {
          // Get device unique id first and then push a heartbeat data.
          var envelope = {
            type: 1
          , ret_msg: {
              identityId: id
            , status: 1
            , screenPort: options.screenPort
            , adbSerial: options.serial
            , appiumPort: options.appiumPort
            }
          }
          
          // log.info(JSON.stringify(envelope))
          push.send([
            options.serial
          , JSON.stringify(envelope)
          ])
        }
        return openService()
      })
      .then(() => {
        // Make /dev/input/ have permission to read.
        adb.shell(options.serial, util.format(
          "am broadcast -a '%s' --es cmd 'chmod 777 /dev/input/*'"
        , apk.broadcast.cmd
        ))

        //Heartbeat per 10 seconds.
        if (heartbeatTimer) {
          clearTimeout(heartbeatTimer)
        } 
        heartbeatTimer = setTimeout(heartbeatTimeout, 10000)

        //Some task need return message.
        for (var e of [
          PROTO.wifi.id.toString()
        , PROTO.airplaneMode.id.toString()
        , PROTO.screenLock.id.toString()
        , PROTO.gps.id.toString()
        , PROTO.clearContacts.id.toString()
        , PROTO.clearImages.id.toString()
        , PROTO.shutdown.id.toString()
        , PROTO.brightness.id.toString()
        , PROTO.uninstallApk.id.toString()
        , PROTO.getAppsList.id.toString()
        , PROTO.getContactsList.id.toString()
        , PROTO.clearCache.id.toString()
        , PROTO.quitApps.id.toString()
        , PROTO.messageBlank.id.toString()
        , PROTO.sendShortMessage.id.toString()
        , PROTO.importContactsByText.id.toString()
        , PROTO.deleteContactById.id.toString()
        , PROTO.getPicturesList.id.toString()
        , PROTO.suShell.id.toString()]){
          router.on(e, (channel, message) => {
            // log.info(JSON.stringify(message))
            runServiceCommand(message)
              .timeout(5000)
              .then(data => {
                push.send([
                  options.serial
                , data
                ])
              })
              .catch((err) => {
                log.error("task", message.id, err.message)
                push.send([
                  channel
                , wireutil.reply(message.id, Number(e)).catchErr(err.message)
                ])
                if (err.name === 'TimeoutError') {
                  log.error("Receive from service timeout, ready kill mspservice.")
                  killService()
                }
              })
          })
        }

        //Import images
        router.on(PROTO.importImages.id.toString(), (channel, message) => {
          var pros = []
          var files = []
          for (f of message.args.files) {
            var resSrc = pathutil.storageFile(f.container, f.name)
            var resDst = '/mnt/sdcard/DCIM/' + f.name 
            files.push(resDst)
            pros.push(adb.push(options.serial, resSrc, resDst)
              .timeout(10000)
              .then(function(transfer) {
                return new Promise(function(resolve, reject) {
                  transfer.on('error', reject)
                  transfer.on('end', resolve)
                })
            }))
          }
          
          Promise.all(pros)
            .then(() => {
                message.args.files = files
                log.info(JSON.stringify(message))
                return runServiceCommand(message)
                // push.send([
                //   options.serial
                // , wireutil.reply(message.id, 1004).okay('Success.')
                // ])
              })
              .timeout(15000)
              .then(data => {
                push.send([
                  options.serial
                , data
                ])
              })
              .catch((err) => {
                log.error("task", message.id, err.toString())
                push.send([
                  channel
                , wireutil.reply(message.id, PROTO.importImages.id).catchErr(err.toString())
                ])
                if (err.name === 'TimeoutError') {
                  log.error("Receive from service timeout, ready kill mspservice.")
                  killService()
                }
              })
        })

        //Some task don't need return message.
        for(var e of [
          PROTO.pressHome.id.toString()
        , PROTO.pressBack.id.toString()
        , PROTO.pressMenu.id.toString()
        , PROTO.inputCharacter.id.toString()
        , PROTO.inputEnter.id.toString()
        , PROTO.inputBackspace.id.toString()
        , PROTO.inputNewline.id.toString()]) {
          router.on(e, (channel, message) => {
            service.socket.write(JSON.stringify(message))
          })
        }

        // Import contact file or install apk.
        for (var e of [
          PROTO.importContactsByFile.id.toString()
        , PROTO.installApk.id.toString()]){
          router.on(e, (channel, message) => {
            var resSrc = pathutil.storageFile(message.args.file.container, message.args.file.name)
            var resDest = '/data/local/tmp/' + message.args.file.name
            adb.push(options.serial, resSrc, resDest)
              .timeout(50000)
              .then(function(transfer) {
                return new Promise(function(resolve, reject) {
                  transfer.on('error', reject)
                  transfer.on('end', resolve)
                })
              })
              .then(() => {
                message.args = { file: resDest}
                log.info(JSON.stringify(message))
                return runServiceCommand(message)  
              })
              .timeout(60000)
              .then(data => {
                push.send([
                  options.serial
                , data
                ])
              })
              .catch((err) => {
                log.error("task", message.id, err)
                push.send([
                  channel
                , wireutil.reply(message.id, Number(e)).catchErr(err.message)
                ])
                if (err.name === 'TimeoutError') {
                  log.error("Receive from service timeout, ready kill mspservice.")
                  killService()
                }
              })
          })
        }

        //Bind virtual identity/device info. 
        router.on(PROTO.bindVirtualInfo.id.toString(), async (channel, message) => {
          try {
            message.args.virtualIdentityInfo.virtualDeviceInfo.virtualIdentityInfoId = message.args.virtualIdentityInfo.id
            await db.virtual_device_info.upsert(message.args.virtualIdentityInfo.virtualDeviceInfo)
            await db.virtual_identity_info.updateAll({id: message.args.virtualIdentityInfo.id}
            , {deviceInfoId: message.devid})
            await db.device_info.updateAll({id: message.devid}
            , {curVirtualIdentityInfoId: message.args.virtualIdentityInfo.id})
            var data = await runServiceCommand(message).timeout(5000)
            if (data != null) {
              push.send([
                options.serial
              , data
              ])
            }
          } catch (err) {
            log.error("task", message.id, err)
            push.send([
              channel
            , wireutil.reply(message.id, PROTO.bindVirtualInfo.id).catchErr(err.message)
            ])
            if (err.name === 'TimeoutError') {
              log.error("Receive from service timeout, ready kill mspservice.")
              killService()
            }
          }
        })

        // Switch virtual identity info.
        router.on(PROTO.switchVirtualInfo.id.toString(), async (channel, message) => {
          try {
            var vdev = await db.virtual_device_info.findOne({where:{virtualIdentityInfoId: message.args.vid}})
            if (!vdev) {
              var errMsg = "Can not find virtual device info by id " + message.args.vid
              log.error("task", message.id, errMsg)
              push.send([
                channel
              , wireutil.reply(message.id, PROTO.switchVirtualInfo.id).fail(errMsg)
              ])
              return
            }

            var vinfo = await db.virtual_identity_info.findById(message.args.vid)
            if (!vinfo) {
              var errMsg = "Can not find virtual identity info by id " + message.args.vid
              log.error("task", message.id, errMsg)
              push.send([
                channel
              , wireutil.reply(message.id, PROTO.switchVirtualInfo.id).fail(errMsg)
              ])
              return
            }

            await db.device_info.updateAll({id: message.devid}
            , {curVirtualIdentityInfoId: message.args.vid})

            vinfo.virtualDeviceInfo = vdev
            message.args = {}
            message.args.virtualIdentityInfo = vinfo
            message.type = PROTO.bindVirtualInfo.id
            log.info(JSON.stringify(message))
            var data = await runServiceCommand(message).timeout(5000)
            if (data != null) {
              push.send([
                options.serial
              , data
              ])
            }
          } catch (err) {
            log.error("task", message.id, err)
            push.send([
              channel
            , wireutil.reply(message.id, PROTO.switchVirtualInfo.id).catchErr(err.message)
            ])
            if (err.name === 'TimeoutError') {
              log.error("Receive from service timeout, ready kill mspservice.")
              killService()
            }
          }
        })

      })
      .return(plugin)
  })
