var syrup = require('stf-syrup')

var devutil = require('../../../../util/devutil')
var logger = require('../../../../util/logger')

module.exports = syrup.serial()
  .dependency(require('../../support/properties'))
  .dependency(require('../../support/adb'))
  .dependency(require('../../support/push'))
  .dependency(require('./display'))
  .define(function(options, properties, adb, push, display) {
    var log = logger.createLogger('device:plugins:identity')

    function solve() {
      log.info('Solving identity')
      var identity = devutil.makeIdentity(options.serial, properties)
      identity.display = display.properties
      return identity
    }

    return solve()
  })
