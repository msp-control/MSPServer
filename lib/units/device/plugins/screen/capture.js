var util = require('util')

var syrup = require('stf-syrup')
var adbkit = require('adbkit')
var fs = require('fs')
var moment = require('moment')

var logger = require('../../../../util/logger')
var wireutil = require('../../../../wire/util')
var pathutil = require('../../../../util/pathutil')

module.exports = syrup.serial()
  .dependency(require('../../support/adb'))
  .dependency(require('../../support/router'))
  .dependency(require('../../support/push'))
  .dependency(require('../../resources/minicap'))
  .dependency(require('../util/display'))
  .define(function(options, adb, router, push, minicap, display) {

    var log = logger.createLogger('device:plugins:screen:capture')
    var plugin = Object.create(null)

    function projectionFormat() {
      return util.format(
        '%dx%d@%dx%d/%d'
      , display.properties.width
      , display.properties.height
      , display.properties.width
      , display.properties.height
      , display.properties.rotation
      )
    }

    plugin.capture = function() {
      // log.info('Capturing screenshot')

      var file = util.format('/data/local/tmp/minicap_%d.jpg', Date.now())
      return minicap.run(util.format(
          '-P %s -s >%s', projectionFormat(), file))
        .then(adbkit.util.readAll)
        .then(function() {
          return adb.stat(options.serial, file)
        })
        .then(function(stats) {
          if (stats.size === 0) {
            throw new Error('Empty screenshot; possibly secure screen?')
          }

          return adb.pull(options.serial, file)
            .then(function(transfer) {
              return new Promise(function(resolve, reject) {
                var fn = util.format('%s_%s.jpg'
                , moment().format('YYYY-MM-DD-HH-mm-ss')
                , options.serial)
                var pfn = pathutil.screenshot(fn)

                // log.info(pfn)

                transfer.on('end', function() {
                  resolve('/images/screenshot/' + fn)
                })
                transfer.on('error', reject)
                transfer.pipe(fs.createWriteStream(pfn))
              })
            })
        })
        .finally(function() {
          return adb.shell(options.serial, ['rm', '-f', file])
            .then(adbkit.util.readAll)
        })
    }

    // Save capture per 3 seconds.
    // function saveCapture(id) {
    //   plugin.capture()
    //     .then((path) => {
    //       var sp = pathutil.apiClient('./' + path)
    //         , dp = pathutil.screens(dev_id + '.jpg')
    //       fs.rename(sp, dp, (err) => {if (err) {log.error(err)}})
    //     })
    // }

    // var db = require('../../../../util/dbutil')
    // var dev_id = -1
    // setInterval(() => {
    //   if (dev_id === -1) {
    //     db.device_info.findOne({fields: ['id'], where: {adb_serial: options.serial}}, (err, model) => {
    //     if (err) {
    //       log.error(err)
    //     } else {
    //       // log.info(model)
    //       if (model !== null) {
    //         dev_id = model.id
    //         saveCapture(dev_id)
    //         }
    //       }
    //     }) 
    //   } else {
    //     saveCapture(dev_id)
    //   }
      
    // }, 3000)


    // Capture task.
    router.on('1013', function(channel, message) {
      plugin.capture()
        .then(function(file) {
          push.send([
            channel
          , wireutil.reply(message.id, 1013).okay(file)
          ])
        })
        .catch(function(err) {
          log.error('Screen capture failed', err.stack)
          push.send([
            channel
          , wireutil.reply(message.id, 1013).fail(err.message)
          ])
        })
    })

    return plugin
  })
