var Promise = require('bluebird')
var syrup = require('stf-syrup')
var logger = require('../../../util/logger')
var EventEmitter = require('eventemitter3')
var wireutil = require('../../../wire/util')
var PROTO = require('../../../util/proto')

module.exports = syrup.serial()
  .dependency(require('../support/adb'))
  .dependency(require('../support/router'))
  .dependency(require('../support/push'))
  .define(function(options, adb, router, push) {

    var log = logger.createLogger('device:plugins:shell')

    router.on(PROTO.shell.id.toString(), function(channel, message) {
      log.info('Running shell command "%s"', message.args.cmd)

      adb.shell(options.serial, message.args.cmd)
        .timeout(10000)
        .then(require('adbkit').util.readAll)
        .then((output) => {
          var out = output.toString().trim()
          // log.info(out) 
          push.send([
            channel
          , wireutil.reply(message.id, 1014).shellMsg(out)
          ])
        })
        .catch((err) => {
          log.error("task", message.id, err)
          push.send([
            channel
          , wireutil.reply(message.id, 1014).catchErr(err)
          ])
        })
    })
  })
