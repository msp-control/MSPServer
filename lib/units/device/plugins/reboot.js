var syrup = require('stf-syrup')

var logger = require('../../../util/logger')
var wireutil = require('../../../wire/util')
var PROTO = require('../../../util/proto')

module.exports = syrup.serial()
  .dependency(require('../support/adb'))
  .dependency(require('../support/router'))
  .dependency(require('../support/push'))
  .define(function(options, adb, router, push) {
    var log = logger.createLogger('device:plugins:reboot')

    router.on(PROTO.reboot.id.toString(), function(channel, message) {
      log.important('Rebooting')

      adb.reboot(options.serial)
        .timeout(30000)
        .then(function() {
          push.send([
            channel
          , wireutil.reply(message.id, PROTO.reboot.id).okay('Reboot success.')
          ])
        })
        .error(function(err) {
          log.error('Reboot failed', err.stack)
          push.send([
            channel
          , wireutil.reply(message.id, PROTO.reboot.id).fail('Reboot failed.'+err.message)
          ])
        })
    })
  })
