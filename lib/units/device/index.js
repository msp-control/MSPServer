var syrup = require('stf-syrup')

var logger = require('../../util/logger')
var lifecycle = require('../../util/lifecycle')
var procutil = require('../../util/procutil')

module.exports = function (options) {
  // Show serial number in logs
  logger.setGlobalIdentifier(options.serial)
  var log = logger.createLogger('device')

  return syrup.serial()
    .define(function (options) {
      var log = logger.createLogger('device')
      log.info('Preparing device')
      return syrup.serial()
        .dependency(require('./plugins/service'))
        .dependency(require('./plugins/screen/stream'))
        .dependency(require('./plugins/screen/capture'))
        .dependency(require('./plugins/reboot'))
        // .dependency(require('./plugins/shell'))
        .dependency(require('./plugins/touch'))
        .define(function (options, stream, capture, service, reboot, touch) {
          if (process.send) {
            // Only if we have a parent process
            process.send('ready')
          }
          log.info('Fully operational')
        })
        .consume(options)
    })
    .consume(options)
    .catch(function (err) {
      log.fatal('Setup had an error', err.stack)
      lifecycle.fatal()
    })
}
