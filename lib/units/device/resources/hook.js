var util = require('util')

var syrup = require('stf-syrup')
var semver = require('semver')

var pathutil = require('../../../util/pathutil')
var streamutil = require('../../../util/streamutil')
var logger = require('../../../util/logger')

module.exports = syrup.serial()
  .dependency(require('../support/adb'))
  .define(function (options, adb) {
    var log = logger.createLogger('device:resources:hook')

    var resource = {
      requiredVersion: '1.0.17'
      , pkg: 'com.rzx.msphook'
      , main: 'com.rzx.msphook.util.Agent'
      , apk: pathutil.vendor('MSPService/MSPHook.apk')
    }

    function getPath() {
      return adb.shell(options.serial, ['pm', 'path', resource.pkg])
        .timeout(10000)
        .then(function (out) {
          return streamutil.findLine(out, (/^package:/))
            .timeout(15000)
            .then(function (line) {
              return line.substr(8)
            })
        })
    }

    function install() {
      log.info('Checking whether we need to install MSPHook')
      return getPath()
        .then(function (installedPath) {
          log.info('Running version check')
          return adb.shell(options.serial, util.format(
            "export CLASSPATH='%s';" +
            " exec app_process /system/bin '%s' --version 2>/dev/null"
            , installedPath
            , resource.main
          ))
            .timeout(10000)
            .then(function (out) {
              return streamutil.readAll(out)
                .timeout(10000)
                .then(function (buffer) {
                  var version = buffer.toString()
                  if (semver.satisfies(version, resource.requiredVersion)) {
                    return installedPath
                  }
                  else {
                    throw new Error(util.format(
                      'Incompatible version %s'
                      , version
                    ))
                  }
                })
            })
        })
        .catch(function () {
          log.info('Installing MSPHook')
          return adb.install(options.serial, resource.apk)
            .timeout(65000)
            .then(function () {
              log.info('Reboot to take effect')
              adb.reboot(options.serial)
            })
        })
    }

    return install()
      .then(function (path) {
        log.info('MSPHook up to date')
        resource.path = path
        return resource
      })
  })
