var util = require('util')

var syrup = require('stf-syrup')
var semver = require('semver')

var pathutil = require('../../../util/pathutil')
var streamutil = require('../../../util/streamutil')
var logger = require('../../../util/logger')

module.exports = syrup.serial()
  .dependency(require('../support/adb'))
  .define(function(options, adb) {
    var log = logger.createLogger('device:resources:service')

    var resource = {
      requiredVersion: '1.0.32'
    , pkg: 'com.rzx.mspservice'
    , main: 'com.rzx.util.Agent'
    , apk: pathutil.vendor('MSPService/MSPService.apk')
    , startIntent: {
        action: 'com.rzx.mspservice.action.START'
      , component: 'com.rzx.mspservice/.MspService'
      }
    , broadcast: {
        version: 'com.rzx.mspservice.action.VERSION'
      , id: 'com.rzx.mspservice.action.ID'
      , display: 'com.rzx.mspservice.action.DISPLAY'
      , kill: 'com.rzx.mspservice.action.KILL'
      , cmd: 'com.rzx.mspservice.action.CMD'
      }
    }

    function getPath() {
      return adb.shell(options.serial, ['pm', 'path', resource.pkg])
        .timeout(10000)
        .then(function(out) {
          return streamutil.findLine(out, (/^package:/))
            .timeout(15000)
            .then(function(line) {
              return line.substr(8)
            })
        })
    }

    function install() {
      log.info('Checking whether we need to install MSPService')
      return getPath()
        .then(function(installedPath) {
          log.info('Running version check')
          return adb.shell(options.serial, util.format(
            "export CLASSPATH='%s';" +
            " exec app_process /system/bin '%s' --version 2>/dev/null"
          , installedPath
          , resource.main
          ))
          .timeout(10000)
          .then(function(out) {
            return streamutil.readAll(out)
              .timeout(10000)
              .then(function(buffer) {
                var version = buffer.toString()
                if (semver.satisfies(version, resource.requiredVersion)) {
                  return installedPath
                }
                else {
                  throw new Error(util.format(
                    'Incompatible version %s'
                  , version
                  ))
                }
              })
          })
        })
        .catch(function() {
          log.info('Installing MSPService')
          // Uninstall first to make sure we don't have any certificate
          // issues.
          return adb.shell(options.serial, util.format(
              "am stopservice --user 0 -n '%s'"
            , resource.startIntent.component
            ))
            .timeout(10000)
            .then(require('adbkit').util.readAll)
            .then((output) => {
              return adb.uninstall(options.serial, resource.pkg)
            })
            .timeout(15000)
            .then(function() {
              return adb.install(options.serial, resource.apk)
                .timeout(65000)
            })
            .then(function() {
              return getPath()
            })
        })
    }

    return install()
      .then(function(path) {
        log.info('MSPService up to date')
        resource.path = path
        return resource
      })
  })
