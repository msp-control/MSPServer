#!/usr/bin/env python
#coding=utf-8
'''
Keep facebook account task script.
'''

import sys
import time
import json
import threading
from selenium.webdriver.support.ui import WebDriverWait
import app.facebook.command as FbCmd
from common.service.dev_service import DevService

server_host = '192.168.57.123'
server_port = 13002

class TaskThread(threading.Thread):
    def __init__(self, task):
        threading.Thread.__init__(self)
        self._task = task

    def run(self):
        cmd = None
        dev_service = DevService(server_host, server_port)

        for acc in self._task:
            try:
                dev_info = acc['dev_info']
                vir_info = acc['vir_info']
                vir_dev_info = acc['vir_dev_info']
                user = acc['user']
                password = acc['password']
                keep_time = acc['keep_time']

                # 连接appium
                cmd = FbCmd.Command(host=server_host, port=dev_info['appium_port'])

                # 切换虚拟身份
                dev_service.task_infos.switch_to_virtual_identity_info(
                    vir_info['id'], vir_info['deviceInfoId'])
                time.sleep(3)

                #Check if device's android_id is effective.
                # android_id = cmd.driver.su_cmd(
                #     "settings get secure android_id")
                # if android_id != vir_dev_info['android_id']:
                #     raise Exception('The device android id is ' + android_id +
                #                     ', not equal to given android id which is '
                #                     + vir_dev_info['android_id'])

                # Step 1: login by cache. If cache file is not exist, it will login by password.
                cmd.driver.open_airplane_mode()
                cmd.driver.close_airplane_mode()
                time.sleep(3)
                cmd.login_by_cache(user, password)
                print user + ' login succeeded.'

                # Step 2: sleep some times, you MUST keep alive with appium in this period.
                try:
                    WebDriverWait(cmd.driver, keep_time, 15).until(
                        lambda x: x.find_element_by_id("keep_alive_id_which_can_NOT_be_found")
                    )
                except:
                    pass

                # Step 3: Save cache. For logining by cache next time.
                cmd.save_cache(user)
                print user + ' save cache succeeded.'
            except Exception, e:
                print user + ' perform keep task failed.' + str(e)

        if cmd:
            time.sleep(10)
            cmd.quit()

def dispatch_task(all_task):
    threads = []
    for one_device_task in all_task:
        thread = TaskThread(all_task[one_device_task])
        thread.start()
        threads.append(thread)

    for thread in threads:
        thread.join()

def generate_task():
    """
    Generate task.
    """

    all_task = {}

    dev_service = DevService(server_host, server_port)
    code, fb_info = dev_service.fb_infos.find()
    if code != 200:
        raise Exception(fb_info)
    fb_info = json.loads(fb_info)

    for acc in fb_info:
        # 根据虚拟身份id获取虚拟身份信息
        code, vir_info = dev_service.virtual_identity_infos.find_by_id(
            acc['virtualIdentityInfoId'])
        if code != 200:
            raise Exception(vir_info)
        vir_info = json.loads(vir_info)

        # 根据虚拟身份id获取虚拟设备信息
        code, vir_dev_info = dev_service.virtual_identity_infos.virtual_device_info(
            acc['virtualIdentityInfoId'])
        if code != 200:
            raise Exception(vir_dev_info)
        vir_dev_info = json.loads(vir_dev_info)

        # 根据虚拟身份信息的绑定真实设备id获取appium服务端口
        code, dev_info = dev_service.device_infos.find_by_id(
            vir_info['deviceInfoId'])
        if code != 200:
            raise Exception(dev_info)
        dev_info = json.loads(dev_info)

        this_task = {
            'user': acc['email'],
            'password': acc['password'],
            'vir_info': vir_info,
            'dev_info': dev_info,
            'vir_dev_info': vir_dev_info,
            'keep_time': 900
        }
        if vir_info['deviceInfoId'] not in all_task:
            all_task[vir_info['deviceInfoId']] = []
        all_task[vir_info['deviceInfoId']].append(this_task)

    return all_task

def main():
    """
    Main function.
    """
    try:
        all_task = generate_task()
        dispatch_task(all_task)
    except Exception, e:
        print str(e)


if __name__ == '__main__':
    main()
