#!/usr/bin/env python
# coding=utf-8

from .mobilecommand import MobileCommand as Command
from appium import webdriver

class WebDriver(webdriver.Remote):
    def __init__(self, command_executor='http://127.0.0.1:4444/wd/hub',
                 desired_capabilities=None, browser_profile=None, proxy=None, keep_alive=False):
        super(WebDriver, self).__init__(command_executor, desired_capabilities, browser_profile, proxy, keep_alive)

    def _addCommands(self):
        super(WebDriver, self)._addCommands()
        self.command_executor._commands[Command.SU_CMD] = \
            ('POST', '/session/$sessionId/appium/suCmd')

    def su_cmd(self, cmd):
        data = {}
        data['cmd'] = cmd
        return self.execute(Command.SU_CMD, data)['value']

    def file_exists(self, path, file):
        ret = self.su_cmd('ls ' + path + ' 2>/dev/null|grep ' + file)
        return ret != ''

    def open_airplane_mode(self):
        self.su_cmd("settings put global airplane_mode_on 1")
        self.su_cmd("am broadcast -a android.intent.action.AIRPLANE_MODE --ez state true")

    def close_airplane_mode(self):
        self.su_cmd("settings put global airplane_mode_on 0")
        self.su_cmd("am broadcast -a android.intent.action.AIRPLANE_MODE --ez state false")
