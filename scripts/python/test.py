#!/usr/bin/env python
#coding=utf-8

import time
import json
import app.facebook.command as FbCmd
from common.service.dev_service import DevService

server_host = '192.168.57.123'
server_port = 13002

likeurl = 'https://m.facebook.com/story.php?story_fbid=107382996474615&id=100016087448479'
commenturl = likeurl
reporturl =  likeurl # 'https://www.facebook.com/nj.radcliffe.7'
cmd = None

try:
  dev_service = DevService(server_host, server_port)
  code, result = dev_service.device_infos.find('{"where":{"status":{"gt":0}}}')
  if code != 200:
      print 'Get device infos failed.'+result
      exit(-1)

  dev_infos = json.loads(result)
  if len(dev_infos) == 0:
      print 'Get 0 device.'
      exit(-1)

  for dev in dev_infos:
    if dev['loc_x'] == 2 and dev['loc_y'] == 1:
      cmd = FbCmd.Command(host='192.168.57.123', port=dev['appium_port'])
      # cmd.login_by_password('LoveKuang1991@gmx.com', 'Llwhnew*8490')
      # cmd.driver.su_cmd("am start -n com.facebook.katana/com.facebook.katana.IntentUriHandler -d '"+likeurl+"'")
      # print 'Login successfully!'
      # cmd.like(likeurl)
      # print 'Like successfully!'
      # cmd.comment(commenturl, '是的')
      # print 'Comment successfully!'
  cmd.report_profile(reporturl)
  print 'Report profile successfully!'
except Exception, e:
  print str(e)

if cmd:
  time.sleep(10)
  cmd.quit()
