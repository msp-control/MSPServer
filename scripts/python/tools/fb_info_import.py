#!/usr/bin/env python
# coding=utf-8

import sys
import json
import urllib
import urllib2

def import_info(data):
    """
    import_info
    """

    host = '192.168.57.123'
    port = 13002
    base_url = 'http://' + host + ':' + str(port) + '/api'

    #获取对应坐标的设备的id
    req_url = base_url + '/device_infos/findOne'
    filters = urllib.quote(
        '{"where":{"loc_x":' +
        str(data['virtualIdentityInfo']['deviceInfo']['loc_x']) + ',"loc_y":' +
        str(data['virtualIdentityInfo']['deviceInfo']['loc_y']) + '}}')
    req_url += '?filter=' + filters
    req = urllib2.Request(req_url)
    res = urllib2.urlopen(req)
    res_data = res.read()
    if res.code != 200:
        raise Exception(res_data)
    dev_id = json.loads(res_data)['id']
    data['virtualIdentityInfo']['deviceInfoId'] = dev_id

    #导入虚拟身份信息
    vir_info_data = urllib.urlencode(data['virtualIdentityInfo'])
    req_url = base_url + '/virtual_identity_infos'
    req = urllib2.Request(req_url, vir_info_data)
    res = urllib2.urlopen(req)
    res_data = res.read()
    if res.code != 200:
        raise Exception(res_data)
    vir_info_id = json.loads(res_data)['id']

    #导入虚拟设备信息
    vir_dev_data = urllib.urlencode(data['virtualIdentityInfo']['virtualDeviceInfo'])
    req_url = base_url + '/virtual_identity_infos/' + str(vir_info_id) + '/virtualDeviceInfo'
    req = urllib2.Request(req_url, vir_dev_data)
    res = urllib2.urlopen(req)
    res_data = res.read()
    if res.code != 200:
        raise Exception(res_data)

    #导入facebook账号信息
    vir_dev_data = urllib.urlencode(data['virtualIdentityInfo']['fbInfo'])
    req_url = base_url + '/virtual_identity_infos/' + str(vir_info_id) + '/fbInfo'
    req = urllib2.Request(req_url, vir_dev_data)
    res = urllib2.urlopen(req)
    res_data = res.read()
    if res.code != 200:
        raise Exception(res_data)

def main():
    """
    main
    """
    file_p = open(sys.argv[1], 'r')
    datas = json.load(file_p)
    file_p.close()

    for data in datas:
        import_info(data)

if __name__ == '__main__':
    if len(sys.argv) < 2:
        print 'Usage: python ' + sys.argv[0] + ' fb_import_info.json'
        exit(1)
    main()
