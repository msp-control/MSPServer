#!/usr/bin/env python
#coding=utf-8

import sys
import os
import time
import traceback
sys.path.append(os.path.split(os.path.realpath('..'))[0])
from ext import webdriver
from utils.command.find import find_webview_in_time, find_ele_or_eles_in_time


class Command(object):
    def __init__(self, host='127.0.0.1', port=4723, desired_caps={}):
        default_desired = {}
        default_desired['platformName'] = 'Android'
        # default_desired['platformVersion'] = '4.4'
        default_desired['deviceName'] = 'Android device'
        default_desired['app'] = 'com.facebook.katana'
        default_desired['appActivity'] = '.LoginActivity'
        default_desired['autoLaunch'] = False
        default_desired['unicodeKeyboard'] = True
        default_desired['resetKeyboard'] = True
        default_desired['fastReset'] = False
        default_desired.update(desired_caps)

        self._pkg = 'com.facebook.katana'
        self._driver = webdriver.Remote(
            'http://' + host + ':' + str(port) + '/wd/hub', default_desired)

    @property
    def driver(self):
        return self._driver

    def login_by_password(self, user, password):
        # Check if is log on.
        def check_is_logon():
            try:
                # Remember password page
                eleDblOn = self._driver.find_element_by_android_uiautomator(
                    'new UiSelector().resourceId("com.facebook.katana:id/dbl_on")'
                )
                eleDblOn.click()
                return True
            except:
                try:
                    # Nearby friends page
                    eleNearbyNotNow = self._driver.find_element_by_android_uiautomator(
                        'new UiSelector().resourceId("com.facebook.katana:id/background_location_resurrection_button_not_now")'
                    )
                    eleNearbyNotNow.click()
                    return True
                except:
                    try:
                        # Main page
                        eleMainTab = self._driver.find_element_by_android_uiautomator(
                            'new UiSelector().resourceId("com.facebook.katana:id/bookmarks_tab")'
                        )
                        eleMainTab.click()
                        return True
                    except:
                        pass
            return False

        self.reset_app()
        self._driver.launch_app()
        time.sleep(10)

        try:
            # User input box.
            eleUser = find_ele_or_eles_in_time(
                self._driver.find_element_by_android_uiautomator,
                ('new UiSelector().className("android.widget.EditText").instance(0)'
                 ), 30)

            # Password input box.
            elePassword = find_ele_or_eles_in_time(
                self._driver.find_element_by_android_uiautomator,
                ('new UiSelector().className("android.widget.EditText").instance(1)'
                 ), 2)

            # Log in button.
            eleLogin = find_ele_or_eles_in_time(
                self._driver.find_element_by_android_uiautomator, (
                    'new UiSelector().text("LOG IN")'), 2)

            eleUser.clear()
            eleUser.set_text(user)
            # To avoid user prompt list cover other elements.
            time.sleep(1)
            self._driver.tap([(110, 410)])
            time.sleep(1)
            elePassword.set_text(password)
            eleLogin.click()

            logonTimeout = 60000
            leftTime = logonTimeout
            tryTimesForBadNet = 3
            ok = True
            errMsg = ''
            while True:
                try:
                    eleTitle = self._driver.find_element_by_android_uiautomator(
                        'new UiSelector().resourceId("com.facebook.katana:id/alertTitle")'
                    )
                    eleMsg = self._driver.find_element_by_android_uiautomator(
                        'new UiSelector().resourceId("com.facebook.katana:id/message")'
                    )

                    title = eleTitle.text
                    msg = eleMsg.text
                    if msg.find('check your internet connection'
                                ) >= 0 and tryTimesForBadNet > 0:
                        tryTimesForBadNet = tryTimesForBadNet - 1
                        try:
                            btnOk = self._driver.find_element_by_android_uiautomator(
                                'new UiSelector().text("OK")')
                            btnOk.click()
                            time.sleep(2)
                            elePassword.set_text(password)
                            eleLogin.click()
                        except:
                            pass
                        continue
                    ok = False
                    errMsg = title + '. ' + msg
                    break
                except:
                    pass

                isLogOn = check_is_logon()
                if isLogOn:
                    self.go_home_page()
                    time.sleep(3)
                    ok = True
                    break

                leftTime = leftTime - 1000
                if leftTime < 0:
                    ok = False
                    errMsg = 'Timeout.'
                    break
                time.sleep(1)
            if not ok:
                # print errMsg
                raise Exception(errMsg)
        except Exception, e:
            # print Exception, ":", e
            raise Exception(user + ' login failed. ' + e.message +
                            traceback.format_exc())

    def login_by_cache(self, user, password):
        cache_path = '/data/data/com.facebook.katana-save_cache'
        cache_path_file = cache_path + '/' + user
        if not self._driver.file_exists(cache_path, user):
            return self.login_by_password(user, password)

        try:
            # self._driver.su_cmd('rm -fr /data/data/' + self._pkg)
            self.reset_app()
            self._driver.su_cmd('mv -f ' + cache_path_file + '/*'
                                ' /data/data/' + self._pkg)
            self._driver.su_cmd(
                'mv -f /data/data/com.facebook.katana-common_cache/* ' +
                ' /data/data/' + self._pkg)

            # 还原facebook uid和gid
            uid = self._driver.su_cmd(
                "cat /data/system/packages.list|grep 'com.facebook.katana '" +
                "|busybox awk '{print $2}'")
            self._driver.su_cmd('busybox chown -H ' + uid + ':' + uid +
                                ' /data/data/' + self._pkg)
            self._driver.launch_app()
            time.sleep(3)
            self.go_home_page()
            ele_main_tab = find_ele_or_eles_in_time(
                self._driver.find_element_by_android_uiautomator,
                ('new UiSelector().resourceId("com.facebook.katana:id/bookmarks_tab")'
                 ), 20)
            ele_main_tab.click()
        except Exception, err:
            return self.login_by_password(user, password)

    def reset_app(self):
        self._driver.su_cmd("am force-stop " + self._pkg)
        app_path = '/data/data/' + self._pkg
        self._driver.su_cmd('rm -fr ' + app_path + '/*')
        # self._driver.su_cmd('mkdir ' + app_path)
        # uid = self._driver.su_cmd(
        #     "cat /data/system/packages.list|grep 'com.facebook.katana '|busybox awk '{print $2}'"
        # )
        # self._driver.su_cmd('busybox chown -H ' + uid + ':' + uid + ' ' +
        #                     app_path)

    def save_cache(self, user):
        common_cache = self._pkg + '-common_cache'
        common_cache_path = '/data/data/' + common_cache
        facebook_path = '/data/data/com.facebook.katana'
        save_cache = self._pkg + '-save_cache/' + user
        save_cache_path = '/data/data/' + save_cache

        self._driver.su_cmd('rm -fr ' + save_cache_path + '&& mkdir -p ' +
                            save_cache_path)
        self._driver.su_cmd('rm -fr ' + common_cache_path + '&& mkdir -p ' +
                            common_cache_path)
        self._driver.su_cmd('mv -f' + facebook_path + '/dex ' + facebook_path +
                            '/lib* ' + common_cache_path)
        self._driver.su_cmd("mv -f " + facebook_path + '/* ' + save_cache_path)
        self.reset_app()

    def go_home_page(self):
        self._driver.su_cmd(
            "am start -n com.facebook.katana/com.facebook.katana.IntentUriHandler -d ''"
        )

    def comment(self, url, content):
        self._driver.su_cmd(
            "am start -n com.facebook.katana/com.facebook.katana.IntentUriHandler -d '"
            + url + "'")
        try:
            commentEdit = find_ele_or_eles_in_time(
                self._driver.find_element_by_android_uiautomator, (
                    'new UiSelector().text("Write a comment…")'), 20)
            commentEdit.set_text(content)
            time.sleep(2)
            btnSend = self._driver.find_element_by_android_uiautomator(
                'new UiSelector().description("Send")')
            btnSend.click()
        except Exception, e:
            # print Exception, ":", e
            raise Exception('Comment "' + url + '" failed.' + e.message)

    def like(self, url, emot=0):
        self._driver.su_cmd(
            "am start -n com.facebook.katana/com.facebook.katana.IntentUriHandler -d '"
            + url + "'")
        try:
            find_ele_or_eles_in_time(
                self._driver.find_element_by_android_uiautomator,
                ('new UiSelector().resourceId("com.facebook.katana:id/comment_post_button")'
                 ), 20)

            # Drag start position.
            eleStart = self._driver.find_element_by_android_uiautomator(
                'new UiSelector().resourceId("com.facebook.katana:id/comment_composer_view")'
            )
            # Drag end position.
            eleEnd = self._driver.find_element_by_android_uiautomator(
                'new UiSelector().resourceId("com.facebook.katana:id/titlebar")'
            )

            startLoc = eleStart.location_in_view
            startSize = eleStart.size
            endLoc = eleEnd.location_in_view
            endSize = eleEnd.size

            try:
                self._driver.find_element_by_android_uiautomator(
                    'new UiSelector().resourceId("com.facebook.katana:id/feed_feedback_like_container")'
                )
            except:
                self._driver.swipe(startLoc.x + (startSize.width / 2),
                                   startLoc.y - 100,
                                   endLoc.x + (endSize.width / 2),
                                   endLoc.y + endSize.height + 200)

            try:
                btnUnlike = self._driver.find_element_by_android_uiautomator(
                    'new UiSelector().descriptionContains("button. Double tap")'
                )
                btnUnlike.click()
            except:
                # Button already liked.
                pass
        except Exception, e:
            # print Exception, ":", e
            raise Exception('Like "' + url + '" failed.' + e.message)

    def reply(self, url, content):
        pass

    def report_post(self, url):
        pass

    def report_profile(self, url):
        self._driver.su_cmd(
            "am start -n com.facebook.katana/com.facebook.katana.IntentUriHandler -d '"
            + url + "'")
        time.sleep(5)
        try:
            # 通过webview方式定位，url直接填写profile的链接地址，该profile页面为webview形式
            # find_webview_in_time(self._driver, 'WEBVIEW_com.facebook.katana',
            #                      15)
            # self._driver.switch_to.context('WEBVIEW_com.facebook.katana')
            # time.sleep(3)
            # eleSeeMore = find_ele_or_eles_in_time(
            #     self._driver.find_element_by_xpath, (
            #         '//*[@id="m-timeline-cover-section"]/div[1]/div[2]/div/div[3]'), 20)
            # eleSeeMore.click()
            # time.sleep(1)
            # eleReport = find_ele_or_eles_in_time(
            #     self._driver.find_element_by_xpath, (
            #         '//*[@id="root"]/div[3]/div/div/div[2]/a[4][span = "Report"]'), 3)
            # eleReport.click()
            # self._driver.switch_to.context('NATIVE_APP')
            # time.sleep(3)
            # eleDone = find_ele_or_eles_in_time(
            #     self._driver.find_element_by_android_uiautomator, (
            #         'new UiSelector().text("Done")'), 20)
            # eleDone.click()

            # 通过本地组件方式定位,url需填写说说的url，然后通过说说的url找到人物头像进入profile界面，该profile为本地组件形式
            eleProfileLink = find_ele_or_eles_in_time(
                self._driver.find_element_by_android_uiautomator,
                ('new UiSelector()' +
                 '.className("android.support.v7.widget.RecyclerView")' +
                 '.childSelector(new UiSelector().instance(0))' +
                 '.childSelector(new UiSelector().instance(0))'), 20)
            eleProfileLink.click()
            time.sleep(3)

            eleSeeMore = find_ele_or_eles_in_time(
                self._driver.find_element_by_android_uiautomator, (
                    'new UiSelector().description("See more actions")'), 20)
            eleSeeMore.click()
            time.sleep(1)
            eleReport = find_ele_or_eles_in_time(
                self._driver.find_element_by_android_uiautomator, (
                    'new UiSelector().text("Report")'), 2)
            eleReport.click()
            time.sleep(3)
            eleDone = find_ele_or_eles_in_time(
                self._driver.find_element_by_android_uiautomator, (
                    'new UiSelector().text("Done")'), 20)
            eleDone.click()
        except Exception, e:
            # print Exception, ":", e
            raise Exception('Like "' + url + '" failed.' + e.message)

    def quit(self):
        self._driver.quit()
