#!/usr/bin/env python
#coding=utf-8
'''
Virtual identity infos interface.
'''
#
# Created at 2017/04/13
#
import urllib
import urllib2


class VirtualIdentityInfos(object):
    """
    Provides virtual identity infos api interfaces.
    """

    def __init__(self, base_url):
        self._base_url = base_url

    def find(self, filters=''):
        """
        Get virtual identity infos.

        :Args:
        - filters: Filter defining fields, where, include, order, offset, and limit.

        :Usage:
            virtual_infos.find('{"fields":["id"],"where":{"version":"21"}')
        """
        req_url = self._base_url + '/virtual_identity_infos'
        if filters != '':
            filters_ = urllib.urlencode(filters)
            req_url += '?filter=' + filters_
        req = urllib2.Request(req_url)
        res = urllib2.urlopen(req)
        return res.code, res.read()

    def find_one(self, filters=''):
        """
        Get virtual identity infos limit one.

        :Args:
        - filters: Filter defining fields, where, include, order, offset, and limit.

        :Usage:
            virtual_infos.find_one('{"fields":["id"],"where":{"version":"21"}')
        """
        req_url = self._base_url + '/virtual_identity_infos/findOne'
        if filters != '':
            filters_ = urllib.urlencode(filters)
            req_url += '?filter=' + filters_
        req = urllib2.Request(req_url)
        res = urllib2.urlopen(req)
        return res.code, res.read()

    def find_by_id(self, id, filters=''):
        """
        Get virtual identity infos by id.

        :Args:
        - id: device id.
        - filters: Filter defining fields, where, include, order, offset, and limit.

        :Usage:
            virtual_infos.find_by_id(1, '{"fields":["group"],"where":{"version":"21"}')
        """
        req_url = self._base_url + '/virtual_identity_infos/' + str(id)
        if filters != '':
            filters_ = urllib.quote(filters)
            req_url += '?filter=' + filters_
        req = urllib2.Request(req_url)
        res = urllib2.urlopen(req)
        return res.code, res.read()

    def virtual_device_info(self, id):
        """
        Get virtual device info by id.

        :Args:
        - id: device id.

        :Usage:
            virtual_infos.virtual_device_info(1)
        """
        req_url = self._base_url + '/virtual_identity_infos/' + str(
            id) + '/virtualDeviceinfo'
        req = urllib2.Request(req_url)
        res = urllib2.urlopen(req)
        return res.code, res.read()
