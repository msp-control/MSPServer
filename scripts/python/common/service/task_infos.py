#!/usr/bin/env python
#coding=utf-8
'''
Task infos interface.
'''
#
# Created at 2017/03/09
#
import urllib
import urllib2
import json

class TaskInfos(object):
    """
    Provides task infos api interfaces.
    """
    def __init__(self, base_url):
        self._base_url = base_url

    def create_task(self, type, devs=[], args={}):
        """
        Create task.

        :Args:
        - type: Task type.
        - devs: Device ids.
        - args: Task arguments.

        :Usage:
            task_infos.create_task(3000, [1], {"char":"test"})
        """
        args_ = json.dumps(args)
        data = urllib.urlencode({"type": type, "devs":devs, "args":args_})
        req = urllib2.Request(self._base_url + '/task_infos/createTask',\
            data=data)
        res = urllib2.urlopen(req)
        return res.code, res.read()

    def input_text(self, id, text):
        """
        Input text into device's inputbox.

        :Args:
        - id: Device id.
        - text: Input content.

        :Usage:
            task_infos.input_text(1, 'Hello!')
        """
        return self.create_task(3000, [id], {"char":text})

    def switch_to_virtual_identity_info(self, id, dev_id = None):
        """
        Switch to virtual identity info.

        :Args:
        - id: Virtual identity info id.
        - dev_id: Device id.

        :Usage:
            task_infos.switch_to_virtual_identity_info(1)
        """
        # Get real device id if dev_id is not given.
        if dev_id is None:
            req = urllib2.Request(self._base_url + '/virtual_identity_infos/' + str(id))
            res = urllib2.urlopen(req)
            if res.code != 200:
                return res.code, res.read()

            vir_info = json.loads(res.read())
            if 'deviceInfoId' not in vir_info.keys():
                return 404, 'Key `deviceInfoId` not in virtual_identity_infos.'
            if vir_info.deviceInfoId is None:
                return 404, 'The value of `deviceInfoId` is None.'
            dev_id = vir_info.deviceInfoId
        return self.create_task(1029, [dev_id], {"vid":id})
        