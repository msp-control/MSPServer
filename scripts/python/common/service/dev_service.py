#!/usr/bin/env python
#coding=utf-8
'''
Device api service interface.
'''
#
# Created at 2017/03/09
#

from .device_infos import DeviceInfos
from .task_infos import TaskInfos
from .virtual_identity_infos import VirtualIdentityInfos
from .fb_infos import FbInfos

class DevService(object):
    """
    Provides services for http api interfaces.
    """
    def __init__(self, host, port, schema='http'):
        self._host = host
        self._port = port
        self._base_url = schema+"://"+host+":"+str(port)+'/api'
        self._device_infos = DeviceInfos(self._base_url)
        self._task_infos = TaskInfos(self._base_url)
        self._virtual_identity_infos = VirtualIdentityInfos(self._base_url)
        self._fb_infos = FbInfos(self._base_url)

    @property
    def device_infos(self):
        return self._device_infos

    @property
    def task_infos(self):
        return self._task_infos

    @property
    def virtual_identity_infos(self):
        return self._virtual_identity_infos

    @property
    def fb_infos(self):
        return self._fb_infos

    @property
    def host(self):
        return self._host

    @property
    def port(self):
        return self._port

    @property
    def base_url(self):
        return self._base_url
