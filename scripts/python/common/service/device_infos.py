#!/usr/bin/env python
#coding=utf-8
'''
Device infos interface.
'''
#
# Created at 2017/03/09
#
import urllib
import urllib2


class DeviceInfos(object):
    """
    Provides device infos api interfaces.
    """

    def __init__(self, base_url):
        self._base_url = base_url

    def find(self, filters=''):
        """
        Get device infos.

        :Args:
        - filters: Filter defining fields, where, include, order, offset, and limit.

        :Usage:
            dev_infos.find('{"fields":["id"],"where":{"status":1}')
        """
        req_url = self._base_url+'/device_infos'
        if filters != '':
            filters_ = urllib.quote(filters)
            req_url += '?filter=' + filters_
        req = urllib2.Request(req_url)
        res = urllib2.urlopen(req)
        return res.code, res.read()

    def find_one(self, filters=''):
        """
        Get device infos limit one.

        :Args:
        - filters: Filter defining fields, where, include, order, offset, and limit.

        :Usage:
            dev_infos.find_one('{"fields":["id"],"where":{"status":1}')
        """
        req_url = self._base_url+'/device_infos/findOne'
        if filters != '':
            filters_ = urllib.quote(filters)
            req_url += '?filter=' + filters_
        req = urllib2.Request(req_url)
        res = urllib2.urlopen(req)
        return res.code, res.read()

    def find_by_id(self, id, filters=''):
        """
        Get device infos by id.

        :Args:
        - id: device id.
        - filters: Filter defining fields, where, include, order, offset, and limit.

        :Usage:
            dev_infos.find_by_id(1, '{"fields":["group"],"where":{"status":1}')
        """
        req_url = self._base_url+'/device_infos/'+str(id)
        if filters != '':
            filters_ = urllib.quote(filters)
            req_url += '?filter=' + filters_
        req = urllib2.Request(req_url)
        res = urllib2.urlopen(req)
        return res.code, res.read()

    def exec_su_cmd(self, id, cmd):
        """
        Execute root command and get result in device by id.

        :Args:
        - id: Device id.
        - cmd: Execute command.

        :Usage:
            dev_infos.exec_su_cmd(1, 'ls /data/data/')
        """
        data = urllib.urlencode({"cmd":cmd})
        req_url = self._base_url+'/device_infos/'+str(id)+'/execSuCmd'
        req = urllib2.Request(req_url, data)
        res = urllib2.urlopen(req)
        return res.code, res.read()

    def get_apps_list(self, id):
        """
        Get app list by id.

        :Args:
        - id: Device id.

        :Usage:
            device_infos.get_apps_list(1)
        """
        req_url = self._base_url+'/device_infos/'+str(id)+'/getAppsList'
        req = urllib2.Request(req_url)
        res = urllib2.urlopen(req)
        return res.code, res.read()

    def get_contacts_list(self, id):
        """
        Get contact list by id.

        :Args:
        - id: Device id.

        :Usage:
            device_infos.get_contacts_list(1)
        """
        req_url = self._base_url+'/device_infos/'+str(id)+'/getContactsList'
        req = urllib2.Request(req_url)
        res = urllib2.urlopen(req)
        return res.code, res.read()

    def get_pictures_list(self, id):
        """
        Get pictures list by id.

        :Args:
        - id: Device id.

        :Usage:
            device_infos.get_pictures_list(1)
        """
        req_url = self._base_url+'/device_infos/'+str(id)+'/getPicturesList'
        req = urllib2.Request(req_url)
        res = urllib2.urlopen(req)
        return res.code, res.read()


