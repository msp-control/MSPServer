#!/usr/bin/env python
#coding=utf-8
'''
facebook infos interface.
'''
#
# Created at 2017/04/18
#
import urllib
import urllib2

class FbInfos(object):
    """
    Provides facebook infos api interfaces.
    """
    def __init__(self, base_url):
        self._base_url = base_url

    def find(self, filters=''):
        """
        Get fb infos.

        :Args:
        - filters: Filter defining fields, where, include, order, offset, and limit.

        :Usage:
            fb_infos.find('{"fields":["id"],"where":{"status":1}')
        """
        req_url = self._base_url+'/fb_infos'
        if filters != '':
            filters_ = urllib.quote(filters)
            req_url += '?filter=' + filters_
        req = urllib2.Request(req_url)
        res = urllib2.urlopen(req)
        data = res.read()
        return res.code, data

    def find_one(self, filters=''):
        """
        Get fb infos limit one.

        :Args:
        - filters: Filter defining fields, where, include, order, offset, and limit.

        :Usage:
            fb_infos.find_one('{"fields":["id"],"where":{"status":1}')
        """
        req_url = self._base_url+'/fb_infos/findOne'
        if filters != '':
            filters_ = urllib.quote(filters)
            req_url += '?filter=' + filters_
        req = urllib2.Request(req_url)
        res = urllib2.urlopen(req)
        data = res.read()
        return res.code, data
