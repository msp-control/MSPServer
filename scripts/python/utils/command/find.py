#!/usr/bin/env python
# coding=utf-8

import time

def find_webview_in_time(driver, context, time_out):
    left_time = time_out
    while True:
        #print driver.contexts
        if len(driver.contexts) >= 2:
            for ctx in driver.contexts:
                if ctx == context:
                    return True
                continue
        time.sleep(1)
        left_time -= 1
        if left_time == 0:
            break
    raise Exception('find_webview_in_time(' + context +') Timeout.')

def find_ele_or_eles_in_time(find_function, args, time_out):
    left_time = time_out
    while True:
        try:
            ele = find_function(args)
            return ele
        except Exception, e:
            #print e
            pass
        time.sleep(1)
        left_time -= 1
        if left_time == 0:
            break
    raise Exception(str(find_function) + str(args)+ 'Timeout.')