let strategy = {
  CLASS_NAME: "class name",
  CSS_SELECTOR: "css selector",
  ID: "id",
  NAME: "name",
  LINK_TEXT: "link text",
  PARTIAL_LINK_TEXT: "partial link text",
  XPATH: "xpath",
  ACCESSIBILITY_ID: "accessibility id",
  ANDROID_UIAUTOMATOR: "-android uiautomator"
};

export default strategy;