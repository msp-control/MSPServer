import { errors, isErrorType } from 'appium-base-driver';

let findElOrElsInTime = async function (findFunc, time) {
  var lefttime = time;
  var args = Array.prototype.slice.call(arguments, 2)

  return new Promise(async function (resolve, reject) {
    var timer = setInterval(async function () {
      lefttime = lefttime - 1000;
      try {
        var ele = await findFunc.apply(null, args)
        clearInterval(timer)
        resolve(ele)
      } catch (error) {
        if (!isErrorType(error, errors.NoSuchElementError)) {
          clearInterval(timer);
          reject(error.message);
        }
        if (lefttime <= 0) {
          clearInterval(timer);
          reject(new errors.TimeoutError(`Could not find element(s) in time by ${args}.`));
        }
      }
    }, 1000);
  })
};

let findWebviewInTime = async function (ctx, time) {
  var lefttime = time;
  var _this = this;

  return new Promise(async function (resolve, reject) {
    var timer = setInterval(
      async function () {
        lefttime = lefttime - 1000;
        let contexts = await _this.getContexts();
        if (contexts.indexOf(ctx) !== -1) {
          clearInterval(timer)
          resolve(true)
        }

        if (lefttime <= 0) {
          clearInterval(timer);
          reject(new errors.TimeoutError(`Could not find webview named ${ctx} in time.`));
        }
      }
      , 1000);
  })
}

export {findElOrElsInTime, findWebviewInTime};