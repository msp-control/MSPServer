var sleep = function (time) {
  return new Promise(function (resolve, reject) {
    var timer = setTimeout(function () {
      clearTimeout(timer);
      resolve();
    }, time);
  })
};

export default sleep