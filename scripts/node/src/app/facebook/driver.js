import AndroidDriver from 'appium-android-driver';
import _ from 'lodash';
import portfinder from 'portfinder';

import log from './logger';
import commands from './commands/index';

class FacebookDriver extends AndroidDriver {
  constructor (opts = {}, shouldValidateCaps = true) {
    // var port = await portfinder.getPortPromise()
    // _.defaults(this.opts, {bootstrapPort: port});
    super(opts, shouldValidateCaps);
    log.debug('Start FacebookDriver.');

    for (let [cmd, fn] of _.pairs(commands)) {
      FacebookDriver.prototype[cmd] = fn;
    }
  }

  async createSession (caps) {
    let defaultCaps = {
      appPackage: 'com.facebook.katana',
      appActivity: '.LoginActivity',
      deviceName: 'Android',
      platformName: 'Android',
      unicodeKeyboard: true,
      resetKeyboard: true,
      autoLaunch: false,
      fastReset: true
    };

    _.defaults(caps, defaultCaps);
    await super.createSession(caps);
  }
}

export default FacebookDriver