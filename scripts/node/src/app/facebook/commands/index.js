import actionsCmds from './actions'
import loginCmds from './login'
import likeCmds from './like'
import commentCmds from './comment'
import reportCmds from './report'

let commands = {};
Object.assign(
  commands,
  actionsCmds,
  loginCmds,
  likeCmds,
  commentCmds,
  reportCmds
);

export default commands;