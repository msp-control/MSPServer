import findElOrElsInTime from '../../../utils/find'
import strategy from '../../../utils/strategy'
import sleep from '../../../utils/sleep'

let commands = {}, helpers = {}, extensions = {};

commands.comment = async function (url, comment) {
  await this.suCmd(`am start -n com.facebook.katana/com.facebook.katana.IntentUriHandler -d '${url}'`);
  try {
    var commentEdit = await findElOrElsInTime(
      this.findElement.bind(this)
      , 15000
      , strategy.ANDROID_UIAUTOMATOR
      , `new UiSelector().text("Write a comment…")`
    );
    await this.setValue(comment, commentEdit.ELEMENT);
    await sleep(2000);
    var btnSend = await this.findElement(
      strategy.ANDROID_UIAUTOMATOR
      , `new UiSelector().description("Send")`
    );
    await this.click(btnSend.ELEMENT);
  } catch (err) {
    throw new Error(`Comment "${url}" failed. ` + err.message);
  }
}


Object.assign(extensions, commands, helpers);
export { commands, helpers };
export default extensions;