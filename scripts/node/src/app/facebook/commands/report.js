import { findElOrElsInTime, findWebviewInTime } from '../../../utils/find'
import strategy from '../../../utils/strategy'
import sleep from '../../../utils/sleep'

let commands = {}, helpers = {}, extensions = {};

commands.reportPost = async function (url) {
  await this.suCmd(`am start -n com.facebook.katana/com.facebook.katana.IntentUriHandler -d '${url}'`);
  try {
    let webCtx = 'WEBVIEW_com.facebook.katana'
    await findWebviewInTime.call(
      this
      , webCtx
      , 15000
    );

    await sleep(5000);
    await this.setContext(webCtx);
    // this.curContext = webCtx;
    // More tab button.
    var eleMore = await this.findElOrEls(
      strategy.XPATH
      , `//*[@id="root"]/div` //`//*[@id="m-timeline-cover-section"]/div[1]/div[2]/div/div[4]`
      , false
    );
    await this.click(eleMore.ELEMENT);
    await sleep(2000);

    // Report menu.
    var eleReport = await this.findElOrEls(
      strategy.XPATH
      , `//*[@id="root"]/div[3]/div/div/div[2]/a[2]/span`
      , false
    );
    let eleText = await this.getText(eleReport.ELEMENT);
    if (!eleText || eleText.indexOf('Report') === -1) {
      throw new Error(`Can't find 'Report' menu.`);
    }
    await this.click(eleReport.ELEMENT);
    return true;
  } catch (err) {
    throw new Error(`Report post "${url}" failed. ` + err.message);
  }
}

commands.reportProfile = async function (url) {

}

Object.assign(extensions, commands, helpers);
export { commands, helpers };
export default extensions;