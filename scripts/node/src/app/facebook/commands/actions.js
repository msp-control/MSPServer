import { errors } from 'appium-base-driver';
import findElOrElsInTime from '../../../utils/find'
import strategy from '../../../utils/strategy'
import sleep from '../../../utils/sleep'

let commands = {}, helpers = {}, extensions = {};

commands.goHomeUI = async function () {
  await this.suCmd(`am start -W -n com.facebook.katana/.LoginActivity`);
}

Object.assign(extensions, commands, helpers);
export { commands, helpers };
export default extensions;