import findElOrElsInTime from '../../../utils/find'
import strategy from '../../../utils/strategy'

let commands = {}, helpers = {}, extensions = {};

commands.like = async function (url) {
  await this.suCmd(`am start -n com.facebook.katana/com.facebook.katana.IntentUriHandler -d '${url}'`);
  try {
    await findElOrElsInTime(
      this.findElement.bind(this)
      , 15000
      , strategy.ANDROID_UIAUTOMATOR
      , `new UiSelector().resourceId("com.facebook.katana:id/comment_post_button")`
    );

    // Drag start position.
    var startEle = await this.findElement(
      strategy.ANDROID_UIAUTOMATOR
      , `new UiSelector().resourceId("com.facebook.katana:id/comment_composer_view")`
    )
    // Drag end position.
    var endEle = await this.findElement(
      strategy.ANDROID_UIAUTOMATOR
      , `new UiSelector().resourceId("com.facebook.katana:id/titlebar")`
    )
    var startLoc = await this.getLocationInView(startEle.ELEMENT)
    var startSize = await this.getSize(startEle.ELEMENT)
    var endLoc = await this.getLocationInView(endEle.ELEMENT)
    var endSize = await this.getSize(endEle.ELEMENT)

    try {
      await this.findElement(
        strategy.ANDROID_UIAUTOMATOR
        , `new UiSelector().resourceId("com.facebook.katana:id/feed_feedback_like_container")`
      );
    } catch (err) {
      await this.drag(
        startLoc.x + (startSize.width / 2)
        , startLoc.y - 100
        , endLoc.x + (endSize.width / 2)
        , endLoc.y + endSize.height + 200
        , 1
      );
    }


    try {
      var btnUnLike = await this.findElement(
        strategy.ANDROID_UIAUTOMATOR
        , `new UiSelector().descriptionContains("button. Double tap")`
      );
      await this.click(btnUnLike.ELEMENT);
    } catch (err) {
      // Button already liked.
    }
    return true;
  } catch (err) {
    throw new Error(`Like "${url}" failed. ` + err.message);
  }
}


Object.assign(extensions, commands, helpers);
export { commands, helpers };
export default extensions;