'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.findWebviewInTime = exports.findElOrElsInTime = undefined;

var _appiumBaseDriver = require('appium-base-driver');

var findElOrElsInTime = async function findElOrElsInTime(findFunc, time) {
  var lefttime = time;
  var args = Array.prototype.slice.call(arguments, 2);

  return new Promise(async function (resolve, reject) {
    var timer = setInterval(async function () {
      lefttime = lefttime - 1000;
      try {
        var ele = await findFunc.apply(null, args);
        clearInterval(timer);
        resolve(ele);
      } catch (error) {
        if (!(0, _appiumBaseDriver.isErrorType)(error, _appiumBaseDriver.errors.NoSuchElementError)) {
          clearInterval(timer);
          reject(error.message);
        }
        if (lefttime <= 0) {
          clearInterval(timer);
          reject(new _appiumBaseDriver.errors.TimeoutError('Could not find element(s) in time by ' + args + '.'));
        }
      }
    }, 1000);
  });
};

var findWebviewInTime = async function findWebviewInTime(ctx, time) {
  var lefttime = time;
  var _this = this;

  return new Promise(async function (resolve, reject) {
    var timer = setInterval(async function () {
      lefttime = lefttime - 1000;
      var contexts = await _this.getContexts();
      if (contexts.indexOf(ctx) !== -1) {
        clearInterval(timer);
        resolve(true);
      }

      if (lefttime <= 0) {
        clearInterval(timer);
        reject(new _appiumBaseDriver.errors.TimeoutError('Could not find webview named ' + ctx + ' in time.'));
      }
    }, 1000);
  });
};

exports.findElOrElsInTime = findElOrElsInTime;
exports.findWebviewInTime = findWebviewInTime;