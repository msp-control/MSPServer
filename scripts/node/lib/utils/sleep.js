"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
var sleep = function sleep(time) {
  return new Promise(function (resolve, reject) {
    var timer = setTimeout(function () {
      clearTimeout(timer);
      resolve();
    }, time);
  });
};

exports.default = sleep;