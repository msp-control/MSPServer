'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _appiumSupport = require('appium-support');

var log = _appiumSupport.logger.getLogger('FacebookDriver');

exports.default = log;