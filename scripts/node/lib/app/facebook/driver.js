'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _defaults2 = require('lodash/object/defaults');

var _defaults3 = _interopRequireDefault(_defaults2);

var _pairs2 = require('lodash/object/pairs');

var _pairs3 = _interopRequireDefault(_pairs2);

var _slicedToArray = function () { function sliceIterator(arr, i) { var _arr = []; var _n = true; var _d = false; var _e = undefined; try { for (var _i = arr[Symbol.iterator](), _s; !(_n = (_s = _i.next()).done); _n = true) { _arr.push(_s.value); if (i && _arr.length === i) break; } } catch (err) { _d = true; _e = err; } finally { try { if (!_n && _i["return"]) _i["return"](); } finally { if (_d) throw _e; } } return _arr; } return function (arr, i) { if (Array.isArray(arr)) { return arr; } else if (Symbol.iterator in Object(arr)) { return sliceIterator(arr, i); } else { throw new TypeError("Invalid attempt to destructure non-iterable instance"); } }; }();

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _get = function get(object, property, receiver) { if (object === null) object = Function.prototype; var desc = Object.getOwnPropertyDescriptor(object, property); if (desc === undefined) { var parent = Object.getPrototypeOf(object); if (parent === null) { return undefined; } else { return get(parent, property, receiver); } } else if ("value" in desc) { return desc.value; } else { var getter = desc.get; if (getter === undefined) { return undefined; } return getter.call(receiver); } };

var _appiumAndroidDriver = require('appium-android-driver');

var _appiumAndroidDriver2 = _interopRequireDefault(_appiumAndroidDriver);

var _portfinder = require('portfinder');

var _portfinder2 = _interopRequireDefault(_portfinder);

var _logger = require('./logger');

var _logger2 = _interopRequireDefault(_logger);

var _index = require('./commands/index');

var _index2 = _interopRequireDefault(_index);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var FacebookDriver = function (_AndroidDriver) {
  _inherits(FacebookDriver, _AndroidDriver);

  function FacebookDriver() {
    var opts = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : {};
    var shouldValidateCaps = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : true;

    _classCallCheck(this, FacebookDriver);

    var _this = _possibleConstructorReturn(this, (FacebookDriver.__proto__ || Object.getPrototypeOf(FacebookDriver)).call(this, opts, shouldValidateCaps));
    // var port = await portfinder.getPortPromise()
    // _.defaults(this.opts, {bootstrapPort: port});


    _logger2.default.debug('Start FacebookDriver.');

    var _iteratorNormalCompletion = true;
    var _didIteratorError = false;
    var _iteratorError = undefined;

    try {
      for (var _iterator = (0, _pairs3.default)(_index2.default)[Symbol.iterator](), _step; !(_iteratorNormalCompletion = (_step = _iterator.next()).done); _iteratorNormalCompletion = true) {
        var _step$value = _slicedToArray(_step.value, 2),
            cmd = _step$value[0],
            fn = _step$value[1];

        FacebookDriver.prototype[cmd] = fn;
      }
    } catch (err) {
      _didIteratorError = true;
      _iteratorError = err;
    } finally {
      try {
        if (!_iteratorNormalCompletion && _iterator.return) {
          _iterator.return();
        }
      } finally {
        if (_didIteratorError) {
          throw _iteratorError;
        }
      }
    }

    return _this;
  }

  _createClass(FacebookDriver, [{
    key: 'createSession',
    value: async function createSession(caps) {
      var defaultCaps = {
        appPackage: 'com.facebook.katana',
        appActivity: '.LoginActivity',
        deviceName: 'Android',
        platformName: 'Android',
        unicodeKeyboard: true,
        resetKeyboard: true,
        autoLaunch: false,
        fastReset: true
      };

      (0, _defaults3.default)(caps, defaultCaps);
      await _get(FacebookDriver.prototype.__proto__ || Object.getPrototypeOf(FacebookDriver.prototype), 'createSession', this).call(this, caps);
    }
  }]);

  return FacebookDriver;
}(_appiumAndroidDriver2.default);

exports.default = FacebookDriver;