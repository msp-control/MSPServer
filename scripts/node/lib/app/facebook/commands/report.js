'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.helpers = exports.commands = undefined;

var _find = require('../../../utils/find');

var _strategy = require('../../../utils/strategy');

var _strategy2 = _interopRequireDefault(_strategy);

var _sleep = require('../../../utils/sleep');

var _sleep2 = _interopRequireDefault(_sleep);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var commands = {},
    helpers = {},
    extensions = {};

commands.reportPost = async function (url) {
  await this.suCmd('am start -n com.facebook.katana/com.facebook.katana.IntentUriHandler -d \'' + url + '\'');
  try {
    var webCtx = 'WEBVIEW_com.facebook.katana';
    await _find.findWebviewInTime.call(this, webCtx, 15000);

    await (0, _sleep2.default)(5000);
    await this.setContext(webCtx);
    // this.curContext = webCtx;
    // More tab button.
    var eleMore = await this.findElOrEls(_strategy2.default.XPATH, '//*[@id="root"]/div' //`//*[@id="m-timeline-cover-section"]/div[1]/div[2]/div/div[4]`
    , false);
    await this.click(eleMore.ELEMENT);
    await (0, _sleep2.default)(2000);

    // Report menu.
    var eleReport = await this.findElOrEls(_strategy2.default.XPATH, '//*[@id="root"]/div[3]/div/div/div[2]/a[2]/span', false);
    var eleText = await this.getText(eleReport.ELEMENT);
    if (!eleText || eleText.indexOf('Report') === -1) {
      throw new Error('Can\'t find \'Report\' menu.');
    }
    await this.click(eleReport.ELEMENT);
    return true;
  } catch (err) {
    throw new Error('Report post "' + url + '" failed. ' + err.message);
  }
};

commands.reportProfile = async function (url) {};

Object.assign(extensions, commands, helpers);
exports.commands = commands;
exports.helpers = helpers;
exports.default = extensions;