'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.helpers = exports.commands = undefined;

var _appiumBaseDriver = require('appium-base-driver');

var _find = require('../../../utils/find');

var _find2 = _interopRequireDefault(_find);

var _strategy = require('../../../utils/strategy');

var _strategy2 = _interopRequireDefault(_strategy);

var _sleep = require('../../../utils/sleep');

var _sleep2 = _interopRequireDefault(_sleep);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var commands = {},
    helpers = {},
    extensions = {};

commands.goHomeUI = async function () {
  await this.suCmd('am start -W -n com.facebook.katana/.LoginActivity');
};

Object.assign(extensions, commands, helpers);
exports.commands = commands;
exports.helpers = helpers;
exports.default = extensions;