'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.helpers = exports.commands = undefined;

var _appiumBaseDriver = require('appium-base-driver');

var _find = require('../../../utils/find');

var _find2 = _interopRequireDefault(_find);

var _strategy = require('../../../utils/strategy');

var _strategy2 = _interopRequireDefault(_strategy);

var _sleep = require('../../../utils/sleep');

var _sleep2 = _interopRequireDefault(_sleep);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var commands = {},
    helpers = {},
    extensions = {};

commands.loginByPassword = async function (user, password) {
  // Check if is log on. 
  async function checkIsLogOn() {
    // Remember password UI
    try {
      var eleDblOn = await this.findElement(_strategy2.default.ANDROID_UIAUTOMATOR, 'new UiSelector().resourceId("com.facebook.katana:id/dbl_on")');
      await this.click(eleDblOn.ELEMENT);
      return true;
    } catch (err) {
      // Nearby friends UI
      try {
        var eleNearByNotNow = await this.findElement(_strategy2.default.ANDROID_UIAUTOMATOR, 'new UiSelector().resourceId("com.facebook.katana:id/background_location_resurrection_button_not_now")');
        await this.click(eleNearByNotNow.ELEMENT);
        return true;
      } catch (err) {
        // Main UI.
        try {
          var eleNearByTurnOn = await this.findElement(_strategy2.default.ANDROID_UIAUTOMATOR, 'new UiSelector().resourceId("com.facebook.katana:id/bookmarks_tab")');
          return true;
        } catch (err) {}
      }
    }

    return false;
  }

  try {
    this.launchApp();
    await (0, _sleep2.default)(15000);

    // User input box.
    var eleUser = await (0, _find2.default)(this.findElement.bind(this), 20000, _strategy2.default.ANDROID_UIAUTOMATOR, 'new UiSelector().className("android.widget.EditText").instance(0)');

    // Password input box.
    var elePassword = await (0, _find2.default)(this.findElement.bind(this), 1000, _strategy2.default.ANDROID_UIAUTOMATOR, 'new UiSelector().className("android.widget.EditText").instance(1)');

    // Log in button.
    var eleLogin = await (0, _find2.default)(this.findElement.bind(this), 1000, _strategy2.default.ANDROID_UIAUTOMATOR, 'new UiSelector().text("LOG IN")');

    await this.setValue(user, eleUser.ELEMENT);
    // To avoid user prompt list cover other elements.
    await (0, _sleep2.default)(1000);
    await this.tap(null, 110, 410);
    await (0, _sleep2.default)(1000);
    await this.setValue(password, elePassword.ELEMENT);
    await this.click(eleLogin.ELEMENT);

    var logonTimeout = 60000;
    var leftTime = logonTimeout;
    var ok, errMsg;
    var tryTimesForBadNet = 3;
    while (true) {
      // Login failed.
      try {
        var eleTitle = await this.findElement(_strategy2.default.ANDROID_UIAUTOMATOR, 'new UiSelector().resourceId("com.facebook.katana:id/alertTitle")');

        var eleMsg = await this.findElement(_strategy2.default.ANDROID_UIAUTOMATOR, 'new UiSelector().resourceId("com.facebook.katana:id/message")');

        var title = await this.getText(eleTitle.ELEMENT);
        var msg = await this.getText(eleMsg.ELEMENT);

        if (msg.indexOf('check your internet connection') && tryTimesForBadNet > 0) {
          tryTimesForBadNet = tryTimesForBadNet - 1;
          try {
            var btnOk = await this.findElement(_strategy2.default.ANDROID_UIAUTOMATOR, 'new UiSelector().text("OK")');
            await this.click(btnOk.ELEMENT);
            await (0, _sleep2.default)(2000);
            await this.setValue(password, elePassword.ELEMENT);
            await this.click(eleLogin.ELEMENT);
          } catch (err) {
            // Non-op
          }
          continue;
        }
        ok = false;
        errMsg = title + '. ' + msg;
        break;
      } catch (err) {
        // Non-op
      }

      var isLogOn = await checkIsLogOn.call(this);
      if (isLogOn) {
        await this.goHomeUI();
        await (0, _sleep2.default)(3000);
        ok = true;
        break;
      }

      leftTime = leftTime - 1000;
      if (leftTime <= 0) {
        ok = false;
        errMsg = 'Timeout.';
        break;
      }
      await (0, _sleep2.default)(1000);
    }

    if (!ok) {
      throw new Error(errMsg);
    }
    return ok;
  } catch (err) {
    throw new Error(user + ' login failed.' + err.message);
  }
};

commands.loginByCache = async function (user, userCache) {};

Object.assign(extensions, commands, helpers);
exports.commands = commands;
exports.helpers = helpers;
exports.default = extensions;