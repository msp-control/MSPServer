'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _actions = require('./actions');

var _actions2 = _interopRequireDefault(_actions);

var _login = require('./login');

var _login2 = _interopRequireDefault(_login);

var _like = require('./like');

var _like2 = _interopRequireDefault(_like);

var _comment = require('./comment');

var _comment2 = _interopRequireDefault(_comment);

var _report = require('./report');

var _report2 = _interopRequireDefault(_report);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var commands = {};
Object.assign(commands, _actions2.default, _login2.default, _like2.default, _comment2.default, _report2.default);

exports.default = commands;