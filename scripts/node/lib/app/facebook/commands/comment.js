'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.helpers = exports.commands = undefined;

var _find = require('../../../utils/find');

var _find2 = _interopRequireDefault(_find);

var _strategy = require('../../../utils/strategy');

var _strategy2 = _interopRequireDefault(_strategy);

var _sleep = require('../../../utils/sleep');

var _sleep2 = _interopRequireDefault(_sleep);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var commands = {},
    helpers = {},
    extensions = {};

commands.comment = async function (url, comment) {
  await this.suCmd('am start -n com.facebook.katana/com.facebook.katana.IntentUriHandler -d \'' + url + '\'');
  try {
    var commentEdit = await (0, _find2.default)(this.findElement.bind(this), 15000, _strategy2.default.ANDROID_UIAUTOMATOR, 'new UiSelector().text("Write a comment\u2026")');
    await this.setValue(comment, commentEdit.ELEMENT);
    await (0, _sleep2.default)(2000);
    var btnSend = await this.findElement(_strategy2.default.ANDROID_UIAUTOMATOR, 'new UiSelector().description("Send")');
    await this.click(btnSend.ELEMENT);
  } catch (err) {
    throw new Error('Comment "' + url + '" failed. ' + err.message);
  }
};

Object.assign(extensions, commands, helpers);
exports.commands = commands;
exports.helpers = helpers;
exports.default = extensions;