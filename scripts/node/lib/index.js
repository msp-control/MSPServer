'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.FacebookDriver = undefined;

var _driver = require('./app/facebook/driver');

var _driver2 = _interopRequireDefault(_driver);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.FacebookDriver = _driver2.default;