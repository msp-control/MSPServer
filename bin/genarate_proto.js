var db = require('../lib/util/dbutil')
var fs = require('fs')

db.task_type.find(function (err, model) {
  if (err) {
    console.log(err)
  } else {
    // console.log(model)
    var proto_data = {}
    model.map(function (data) {
      var name = ''
      var needUpper = false
      for(var i = 0; i < data.name.length; ++i) {
        if (data.name[i] === ' ') {
          needUpper = true
          continue
        }

        if (needUpper) {
          name += data.name[i].toUpperCase()
          needUpper = false
        } else {
          name += data.name[i]
        }
      }
      proto_data[name] = data
    })
    var proto_str = 'var PROTO = ' + JSON.stringify(proto_data) + '\n'
    proto_str += 'module.exports = PROTO'
    fs.writeFileSync('../lib/util/proto.js', proto_str)
    // console.log(proto_str)
    process.exit(0)
  }
})