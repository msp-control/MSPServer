module.exports = {
  /**
   * Application configuration section
   * http://pm2.keymetrics.io/docs/usage/application-declaration/
   */
  apps : [

    // MSP local
    // {
    //  name      : "MSP",
    //  script    : "msp",
    //  args      : "local",
    //  node_args : "--harmony-async-await",
    //  env: {
    //    COMMON_VARIABLE: "true"
    //  },
    //  env_production : {
    //    NODE_ENV: "production"
    //  }
    // }

    // MSP api 
    {
      name      : "MSP API",
      script    : "msp",
      args      : "api",
      node_args : "--harmony-async-await",
      exec_mode : 'cluster',
      max_memory_restart: '1024M',
      instances : 0,
      "kill_timeout": 10000
    },

    // MSP websocket
    {
      name      : "MSP WEBSOCKET",
      script    : "msp",
      args      : "websocket",
      node_args : "--harmony-async-await",
      max_memory_restart: '1024M',
      "kill_timeout": 10000
    },

    // MSP app proxy 
    {
      name      : "MSP APP PROXY",
      script    : "msp",
      args      : [
        "proxy",
        "--bind-dealer", "ipc:///tmp/app_proxy_dealer",
        "--bind-pub", "ipc:///tmp/app_proxy_pub",
        "--bind-pull", "ipc:///tmp/app_proxy_pull"
      ],
      node_args : "--harmony-async-await",
      "kill_timeout": 10000
    },

    // MSP dev proxy
    {
      name      : "MSP DEV PROXY",
      script    : "msp",
      args      : [
        "proxy",
        "--bind-dealer", "ipc:///tmp/dev_proxy_dealer",
        "--bind-pub", "ipc:///tmp/dev_proxy_pub",
        "--bind-pull", "ipc:///tmp/dev_proxy_pull"
      ],
      node_args : "--harmony-async-await",
      "kill_timeout": 10000
    },

    // MSP provider
    {
      name      : "MSP PROVIDER",
      script    : "msp",
      args      : "provider",
      node_args : "--harmony-async-await",
      "kill_timeout": 30000
    },

    // MSP processor
    {
      name      : "MSP PROCESSOR",
      script    : "msp",
      args      : "processor",
      node_args : "--harmony-async-await",
      exec_mode : 'cluster',
      instances : 0,
      "kill_timeout": 10000
    }
  ],

  /**
   * Deployment section
   * http://pm2.keymetrics.io/docs/usage/deployment/
   */
  deploy : {
    production : {
      user : "node",
      host : "212.83.163.1",
      ref  : "origin/master",
      repo : "git@github.com:repo.git",
      path : "/var/www/production",
      "post-deploy" : "npm install && pm2 startOrRestart ecosystem.json --env production"
    },
    dev : {
      user : "node",
      host : "212.83.163.1",
      ref  : "origin/master",
      repo : "git@github.com:repo.git",
      path : "/var/www/development",
      "post-deploy" : "npm install && pm2 startOrRestart ecosystem.json --env dev",
      env  : {
        NODE_ENV: "dev"
      }
    }
  }
}
